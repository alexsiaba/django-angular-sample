from django.conf.urls import url
import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login$', views.login_user, name='login'),
    url(r'^logout$', views.logout_user, name='logout'),
    url(r'^signup$', views.signup_user, name='signup'),



    #url(r'migrate_old_data', views.migrate_old_data)
]