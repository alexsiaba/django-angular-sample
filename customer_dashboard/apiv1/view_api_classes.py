import json
import logging
from customer_dashboard.dashboard.models import *

logger = logging.getLogger(__name__)

class Api(object):
    """
    Interface for all API classes
    """

    def __init__(self, request, config):
        self._request = request
        self._request_body = json.loads(self._request.body) if self._request.body else None

        company_id = config.get('company_id', None)
        self._company = Company.objects.get(pk=company_id) if company_id else None

        company_object = config.get('company_object', None)
        self._company_object = eval(company_object.capitalize()) if company_object else None

        logger.debug("API initialized with company id %s" % company_id)

    def get(self):
        raise NotImplementedError

    def post(self):
        raise NotImplementedError

    def put(self):
        raise NotImplementedError

    def delete(self):
        raise NotImplementedError

    @classmethod
    def raise_http_error(cls, status_code, error_message):
        return cls._response(dict(msg=error_message), status_code)

    @classmethod
    def _response(cls, response, status_code=200):
        return status_code, response


class CompanyApi(Api):
    def __init__(self, request, config):

        super(CompanyApi, self).__init__(request, config)
        if self._company_object is not Device:
            self._objects = self._company_object.objects.filter(company=self._company) if self._company_object else []
        self._request_items = self._request_body['items'] if self._request_body and self._company_object else []

        company_object_id = config.get('company_object_id', None)
        if config.get('company_object', None) == 'device':
            self._company_object_id = company_object_id.strip('/')
        else:
            self._company_object_id = uuid.UUID(company_object_id[0:36]) if company_object_id else None

        logger.debug("Company API initialized")

    def get(self):

        if self._company is None:
            return self.raise_http_error(404, 'Company ID must be provided!')

        if self._company_object is None:
            return self._response(dict(returning='company'))

        if self._company_object_id is None:
            return self._response(dict(returning='company_object_collection'))
        else:
            return self._response(dict(returning='company_object'))


    def post(self):

        if self._company is None:
            self._company = Company.build_from_request(self._request_body, self._request.user)
            return self._response(self._company.to_dict())

        if self._company_object is Device:
            for company_device in CompanyHasDevice.objects.filter(company=self._company):
                company_device.delete(hard_delete=True)

        if self._company_object:
            #TODO this is unefficient... We already have company, why retrieving it from db again?
            items = [dict(item, **{'company_id': self._company.id}) for item in self._request_items]
            response = dict(items=[])
            for item in items:
                response_item = self._company_object.build_from_dictionary(item).to_dict()
                response_item.pop('company', None)
                response['items'].append(response_item)
            return self._response(response)

        else:
            return self.raise_http_error(404, 'Unknown server action requested')

    def put(self):

        if self._company is None:
            return self.raise_http_error(404, 'Company ID must be provided')

        if self._company_object is None:
            self._company = self._company.build_from_dictionary(self._request_body)
            return self._response(self._company.to_dict())

        if self._company_object:
            response = dict(items=[])
            objects = {str(obj.id): obj for obj in self._objects}
            for item in self._request_items:
                object = objects.get(item['id'], None)
                if object is not None:
                    object.update_from_dictionary(item)
                    response['items'].append(object.to_dict())
            return self._response(response)

        else:
            return self.raise_http_error(400, 'Undefined PUT request')

    def delete(self):

        if self._company is None:
            return self.raise_http_error(404, 'Company ID must be provided')

        if self._company_object is None:
            self._company.delete()
            return self._response(dict(id=self._company.id))

        if self._company_object and self._company_object_id:
            if self._company_object is Device: #TODO rewrite this
                device = self._company_object.objects.get(pk=self._company_object_id)
                for company_device in CompanyHasDevice.objects.filter(company=self._company, device=device): #TODO there should be only one device (how about past or future? Need to discuss)
                    company_device.delete(hard_delete=True)
                response = dict(device_id=str(device.id), company_id=str(self._company.id))
            else:
                object = self._company_object.objects.get(pk=self._company_object_id, company=self._company)
                object.delete()
                response = dict(id=str(object.id))
            return self._response(response)


class CompanyObjectApi(Api):
    def __init__(self, request, config):

        super(CompanyObjectApi, self).__init__(request, config)

        # company_object_id (employee|department|device) will always exist, see urls for reference
        company_object_id = config.get('company_object_id')
        self._object_instance = self._company_object.objects.get(pk=company_object_id, company=self._company)

        self._action = config.get('company_object_action', None)
        self._action_parameters = config.get('company_object_action_parameters', None)

    def get(self):

        response_handler = getattr(self._object_instance, 'get_%s' % self._action)
        response = response_handler(self._object_instance, self._action_parameters)
        return self._response(response)


    def post(self):

        response_handler = getattr(self._object_instance, 'create_%s' % self._action)
        shifts = self._request_body.get('items', None)
        #checking if the collection or a single element was sent
        if shifts:
            response = dict(items=[])
            for shift in shifts:
                shift['date'] = self._action_parameters
                response['items'].append(response_handler(shift, self._company, self._object_instance))
        else:
            self._request_body['date'] = self._action_parameters
            response = response_handler(self._request_body, self._company, self._object_instance)
        return self._response(response)


    def put(self):

        response_handler = getattr(self._object_instance, 'update_%s' % self._action)
        self._request_body['date'] = self._action_parameters
        response = response_handler(self._request_body, self._company, self._object_instance)
        return self._response(response)




    def delete(self):
        """
        rewrite this!!! easydirtyfix for right now
        :return:
        """

        response, status = getattr(self._object_instance, 'delete_%s' % self._action)(self._action_parameters)
        return self._response(response, status)

        shift = Shift.objects.get(id=self._action_parameters,
                                  department=self._object_instance)
        shift.delete()
        return self._response(dict(id=str(shift.id)))

class ShiftApi(Api):
    def __init__(self, request, config):
        super(ShiftApi, self).__init__(request, config)
        self._shift_id = config.get('shift_id', None)

    def get(self):
        shifts = Shift.objects.filter(id=self._shift_id)
        if len(shifts) == 1:
            shift_formatter = ShiftFormatter()
            return self._response(dict(shift_formatter.format(shifts[0])), 200)
        else:
            return self.raise_http_error(404, 'Shift id not found.')

    def post(self):
        raise NotImplementedError

    def put(self):
        raise NotImplementedError

    def delete(self):
        raise NotImplementedError

class ShiftsByDateApi(Api):

    def __init__(self, request, config):
        super(ShiftsByDateApi, self).__init__(request, config)
        self._start_date = config.get('start_date', None)
        self._end_date = config.get('end_date', self._start_date)

    def get(self):

        #TODO this should go to Shift
        format="%Y-%m-%d"
        start_date = datetime.strptime(self._start_date, format).replace(tzinfo=pytz.utc)

        format="%Y-%m-%d %H:%M:%S"
        end_date = datetime.strptime('%s 23:59:59' % self._end_date, format).replace(tzinfo=pytz.utc)

        shifts = Shift.objects.filter(start_time__gte=start_date, end_time__lte=end_date)
        shift_formatter = ShiftFormatter()

        return_shifts = []
        for shift in shifts:
            shift_dict = shift_formatter.format(shift)
            return_shifts.append(shift_dict)

        return self._response(dict(items=return_shifts))

    def post(self):
        raise NotImplementedError

    def put(self):
        raise NotImplementedError

    def delete(self):
        raise NotImplementedError


class DeviceApi(Api):

    def __init__(self, request, config):
        super(DeviceApi, self).__init__(request, config)
        self._device_id = config.get('device_id', None)
        self._config = config



    def get(self):
        raise NotImplementedError

    def post(self):

        id = self._request_body.get('serial_number', None)
        device_name = self._request_body.get('device_id', None)

        if id and device_name:
            device = Device(id=id, name=device_name)
            device.save()
            response = device.to_dict(),
        else:
            response = dict(msg="Either serial number or the name were not provided"), 400

        return self._response(*response)


    def put(self):

        if self._device_id:
            device = Device.objects.get(pk=self._device_id)
            device.update_from_dictionary(self._request_body)
            response = dict(device=device.to_dict()),
        else:
            response = dict(msg="Device was not found"), 404
        return self._response(*response)


    def delete(self):
        raise NotImplementedError

class ShiftFormatter():
    def format(self, shift):
        try:
            job = Job.objects.get(pk=shift.job_id)
        except ObjectDoesNotExist:
            job = Job(id=2, name='DefaultJob')
        return {
            "id": str(shift.id),
            "shift_id": str(shift.id),
            "company_id": str(shift.employee.company.id),
            "company_name": shift.employee.company.name,
            "employee_id": str(shift.employee.id),
            "employee_name": shift.employee.get_name(),
            "department_id": str(shift.department.id),
            "department_name": shift.department.name,
            "job_id": str(job.id),
            "job_name": job.name,
            "device_id": str(shift.back_device.id),
            "device_name": shift.back_device.name,
            "start_time": shift.start_time.strftime('%Y-%m-%d %H:%M:%S.%f'),
            "end_time": shift.end_time.strftime('%Y-%m-%d %H:%M:%S.%f')
        }
