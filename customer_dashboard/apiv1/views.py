from view_api_classes import *
from django.views.decorators.csrf import csrf_exempt
from customer_dashboard.dashboard.views import login_required
from django.shortcuts import HttpResponse
from django.db import IntegrityError


def api_wrapper(api_class, admins_only=False):
    """
    define api class that will handle all requests
    :param api_class: Whatever_Api class that inherits from Api
    :return: view function decorator
    """
    def view_func_wrapper(view_func):

        """
        Setting common rules for the api view
        :param view_func: view function being decorated
        :return:
        """

        @csrf_exempt
        #@login_required(admins_only)
        def wrapper_func(request, **kwargs):
            try:
                api = api_class(request, kwargs)
                if request.method == 'GET':
                    status_code, response = api.get()

                elif request.method == 'POST':
                    status_code, response = api.post()

                elif request.method == 'PUT':
                    status_code, response = api.put()

                elif request.method == 'DELETE':
                    status_code, response = api.delete()

                else:
                    status_code, response = api.raise_http_error(405, 'Method not allowed!')

            #except ValueError as e:
            #    status_code, response = api_class.raise_http_error(400, 'Malformed request, '
            #                                                            'expecting well-formated json (%s)' % e.message)
            except KeyError as e:
                status_code, response = api_class.raise_http_error(400, 'Some required fields in the request '
                                                                        'were missing or incorrect <Link> '
                                                                        '(%s)' % e.message)
            #except ObjectDoesNotExist as e:
            #    status_code, response = api_class.raise_http_error(404, 'Could not find the specified object '
            #                                                            '(%s)' % e.message)

            except IntegrityError as e:
                status_code, response = api_class.raise_http_error(405, 'Duplicate value (%s)' % e.message)

            return HttpResponse(json.dumps(response), content_type='application/json', status=status_code)
        return wrapper_func
    return view_func_wrapper



@api_wrapper(api_class=CompanyApi)
def company():
    pass

@api_wrapper(api_class=CompanyObjectApi)
def company_object_action():
    pass

@api_wrapper(api_class=ShiftApi)
def shift():
    pass

@api_wrapper(api_class=ShiftsByDateApi)
def shifts_by_date():
    pass

@api_wrapper(api_class=DeviceApi, admins_only=True)
def device():
    pass
