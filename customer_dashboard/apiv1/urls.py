from django.conf.urls import url
import views

urlpatterns = [
    url(r'^company/$', views.company, name='apiv1_company'),

    url(r'^company/'
        r'(?P<company_id>[a-zA-Z0-9\-]{36})/$',
        views.company,
        name='apiv1_company'),

    url(r'^company/'
        r'(?P<company_id>[a-zA-Z0-9\-]{36})/'
        r'(?P<company_object>employee|department|device)/'
        r'(?P<company_object_id>[a-zA-Z0-9\-]{0,36}/*)$',
        views.company,
        name='apiv1_company'),


    url(r'^company/'
        r'(?P<company_id>[a-zA-Z0-9\-]{36})/'
        r'(?P<company_object>department)/'
        r'(?P<company_object_id>[a-zA-Z0-9\-]{36})/'
        r'(?P<company_object_action>schedule|shift)/'
        r'(?P<company_object_action_parameters>\d{4}-\d{2}-\d{2}|[a-zA-Z0-9\-]{36})/$', #parameters could be changed in the future
        views.company_object_action,
        name='apiv1_schedule'),


    url(r'^shift/'
        r'(?P<shift_id>[a-zA-Z0-9\-]{36})/$',
        views.shift,
        name='apiv1_get_shift'),

    #devices
    url(r'^device/$',
        views.device,
        name='post_to_create_new_devices'),

    #url(r'^device/'
    #    r'(?P<device_name>[\d\w]{2}\-\d{4})$',
    #    views.device,
    #    name='device_by_name'),

    url(r'^device/'
        r'(?P<device_id>[A-Z0-9]{16})/$',
        views.device,
        name='device_by_edison_id'),



    url(r'^shifts_by_date/'
        r'(?P<start_date>\d{4}-\d{2}-\d{2})/'
        r'(?P<end_date>\d{4}-\d{2}-\d{2})/$',
        views.shifts_by_date,
        name='apiv1_query_shifts_by_dates'),

    url(r'^shifts_by_date/'
        r'(?P<start_date>\d{4}-\d{2}-\d{2})/$',
        views.shifts_by_date,
        name='apiv1_query_shifts_by_dates'),

    # url(r'^shift/'
    #     r'(?P<shift_id>[a-zA-Z0-9\-]{36})/'
    #     r'(?P<shift_action>processed|failed)/$',
    #     views.shift_notification,
    #     name='apiv1_shift_procession_notification')

    # url(r'^shift/'
    #     r'(?P<shift_id>[a-zA-Z0-9\-]{36})/'
    #     r'(?P<shift_action>processed|failed)/$',
    #     views.shift_notification,
    #     name='apiv1_shift_procession_notification')
]
