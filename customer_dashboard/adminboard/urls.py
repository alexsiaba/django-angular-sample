from django.conf.urls import url
import views

urlpatterns = [
    url(r'^$', views.index, name='admin_dashboard'),
    url(r'^companies/$', views.companies, name='admin_dashboard_companies'),
    url(r'^devices/$', views.devices, name='admin_dashboard_devices'),
]
