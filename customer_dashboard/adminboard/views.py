import json
from django.shortcuts import render_to_response, redirect, RequestContext
from customer_dashboard.dashboard.models import User, Company, CompanyHasUser, Device, CompanyHasDevice
from django.contrib.auth.decorators import user_passes_test


def admins_only(view_func):
    def is_admin(user):
        if user.is_staff and user.is_superuser:
            return True

    @user_passes_test(is_admin, login_url='/', redirect_field_name='to')
    def view_wrapper(request, *args, **kwargs):
        template, context = view_func(request, *args, **kwargs)
        return render_to_response(template, context, context_instance=RequestContext(request))

    return view_wrapper


@admins_only
def index(request):
    return 'adminboard/base.html', {}


@admins_only
def companies(request):
    companies = Company.objects.all()
    for company in companies:
        company.devices = CompanyHasDevice.objects.filter(company=company)

    all_devices = Device.objects.all().order_by('-created_at')
    devices = {
        "back": [device.to_dict() for device in all_devices if not device.wrist],
        "wrist": [device.to_dict() for device in all_devices if device.wrist]
    }
    return 'adminboard/companies.html', dict(companies=json.dumps([c.to_dict() for c in companies]), devices=json.dumps(devices))


@admins_only
def devices(request):
    all_devices = Device.objects.all().order_by('-created_at')
    devices = {
        "back": [device.to_dict() for device in all_devices if not device.wrist],
        "wrist": [device.to_dict() for device in all_devices if device.wrist]
    }

    return 'adminboard/devices.html', dict(devices=json.dumps(devices))
