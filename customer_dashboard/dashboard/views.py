import json
from django.shortcuts import render_to_response, redirect, RequestContext
from django.contrib.auth.decorators import login_required as login
from django.views.decorators.csrf import ensure_csrf_cookie
from models import *
from customer_dashboard.helpers import timezone_mapper

from django.contrib.auth.models import AnonymousUser
from django.contrib.auth import login as log_in


def login_required(view_func):
    """
    TODO rewrite this authorization function completely to accept and validate http_auth
    :param view_func:
    :return:
    """
    def _check_for_api_auth(request, *args, **kwargs):

        http_auth = request.META.get('HTTP_AUTHORIZATION', None)

        # TODO (remove me) start ups pilot
        if request.user.is_authenticated() is False:
            user = User.objects.get(email='ups@ups.com')
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            log_in(request, user)
        # end UPS pilot



        if http_auth == 'kinetic':  #TODO write something more clever
            return view_func(request,  *args, **kwargs)
        else:
            @login(redirect_field_name='to', login_url='/')
            def _wrapper_for_login(request, *args, **kwargs):
                return view_func(request, *args, **kwargs)
            return _wrapper_for_login(request, *args, **kwargs)

    return _check_for_api_auth


def user_should_be_assigned_to_company(view_func):
    def _wrapped_view(request, *args, **kwargs):
        user_in_companies = CompanyHasUser.objects.filter(user=request.user)
        if user_in_companies:
            return view_func(request,  *args, **kwargs)
        else:
            return redirect(company, *args, **kwargs)
    return _wrapped_view


@login_required
@ensure_csrf_cookie
@user_should_be_assigned_to_company
def index(request):
    return company(request)


@login_required
@ensure_csrf_cookie
def company(request, company_id=None):

    context = Company.get_extended_object(user=request.user, company_id=company_id)
    timezones = map(timezone_mapper, [tz[0] for tz in Company.TIMEZONES])
    context['timezones'] = ["%s %s%02d:%02d" % tz for tz in timezones]

    context = {key: json.dumps(value) for key, value in context.iteritems()}

    return render_to_response('company/base.html', context=context, context_instance=RequestContext(request))


@login_required
@ensure_csrf_cookie
@user_should_be_assigned_to_company
def department(request, department_id=None, start_day=None, end_day=None):

    context = Department.get_bound_company(user=request.user,
                                           department_id=department_id,
                                           with_metrics=True,
                                           start_day=start_day,
                                           end_day=end_day)

    context['company_metrics']['user_start_day'] = start_day
    context['company_metrics']['user_end_day'] = end_day or context['company_metrics']['end_date']
    context['company'] = json.dumps(context['company'])
    context['user_companies'] = json.dumps(context['user_companies'])
    context['company_metrics'] = json.dumps(context['company_metrics'])

    return render_to_response('department/base.html', context=context, context_instance=RequestContext(request))


@login_required
@ensure_csrf_cookie
@user_should_be_assigned_to_company
def top_rankings(request, **kwargs):
    return render_to_response('top_rankings/base.html', context_instance=RequestContext(request))
