from django.db import models
from django.db.models.manager import Manager


class MetricsManager(Manager):
    def __init__(self, fields):
        self._explicit_select_fields = fields
        super(MetricsManager, self).__init__()

    def get_queryset(self):
        return super(MetricsManager, self)\
            .get_queryset()\
            .values(*self._explicit_select_fields).using('metrics')


class LiftsView(models.Model):


    shift_id = models.CharField(max_length=255)
    job_name = models.CharField(max_length=255)
    company_id = models.CharField(max_length=255)
    company_name = models.CharField(max_length=255)
    employee_id = models.CharField(max_length=255)
    employee_name = models.CharField(max_length=255)
    shift_start = models.IntegerField()
    shift_end = models.IntegerField()
    lift_start = models.IntegerField()
    lift_end = models.IntegerField()
    back_angle = models.FloatField()
    shift_start_year = models.IntegerField()
    shift_start_month = models.IntegerField()
    shift_start_day = models.IntegerField()
    true_shift_length = models.IntegerField()

    objects = MetricsManager(('shift_id',
                              'job_name',
                              'company_id',
                              'company_name',
                              'employee_id',
                              'employee_name',
                              'shift_start',
                              'shift_end',
                              'lift_start',
                              'lift_end',
                              'back_angle',
                              'shift_start_year',
                              'shift_start_month',
                              'shift_start_day',
                              'true_shift_length'))

    class Meta:
        db_table = 'v_lifts'
        managed = False
