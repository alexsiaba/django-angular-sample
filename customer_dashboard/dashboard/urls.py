from django.conf.urls import url
import views

urlpatterns = [
    url(r'^$', views.index, name='department'),
    url(r'^company/$', views.company, name='company'),
    url(r'^company/([A-Za-z0-9\-]{36})/$', views.company, name='company'),
    url(r'^department/$', views.department, name='department'),

    url(r'^department/'
        r'(?P<department_id>[A-Za-z0-9\-]{36})/$', views.department, name='department'),

    url(r'^department/'
        r'(?P<department_id>[A-Za-z0-9\-]{36})/'
        r'(?P<end_day>\d{4}-\d{2}-\d{2})/$', views.department, name='department'),

    url(r'^department/'
        r'(?P<department_id>[A-Za-z0-9\-]{36})/'
        r'(?P<start_day>\d{4}-\d{2}-\d{2})/'
        r'(?P<end_day>\d{4}-\d{2}-\d{2})/$', views.department, name='department'),

    url(r'^top_rankings/$', views.top_rankings, name='top_rankings'),
]