from django.test import TestCase
from models import *

class UserTestCase(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_user_for_non_empty_email(self):

        with self.assertRaises(ValueError):
            User.objects.create_user(email='good@email.com', password='1234')

        with self.assertRaises(ValidationError):
            User.objects.create_user(email='wrongEmail@garbagehost', password='12345')


class CompanyTestCase(TestCase):

    _user = None
    _company = None

    def setUp(self):
        request_body = {
            "name" : "Dummy Company"
        }
        self._user = User.objects.create_user(email='dummy@email.com', password='password')
        self._company = Company.build_from_request(request_body,request_user=self._user)


    def test_new_company_creates_many_to_many_relations(self):

        many_to_many = CompanyHasUser.objects.filter(company=self._company, user=self._user)
        self.assertTrue(len(many_to_many) > 0)

        extended_company_object = Company.get_extended_object(self._user, self._company.id)
        self.assertIn('company', extended_company_object)
        self.assertIn('user_companies', extended_company_object)
        for key in ['settings', 'departments', 'employees', 'devices', 'jobs']:
            self.assertIn(key, extended_company_object['company'])


    def test_to_dict(self):
        base_test = Base()
        self.assertIsInstance(base_test.id, uuid.UUID)
        base_test.created_at = datetime.now(tz=pytz.utc)



class ShiftTestCase(TestCase):

    _company = None
    _department = None
    _employee = None
    _back_device = None

    def setUp(self):
        request_body = {
            "name" : "Dummy Company",
            "timezone": "US/Eastern"
        }
        self._user = User.objects.create_user(email='dummy@email.com', password='password')
        self._company = Company.build_from_request(request_body,request_user=self._user)
        self._department = Department.objects.create(name="Test Dept", company=self._company)
        self._employee = Employee.objects.create(company=self._company, given_name="Test Employee")
        self._back_device = Device.objects.create(company=self._company, name='Test Device', serial="Test Edison")


    def test_if_dst_saves_correctly(self):
        request_body = dict(
            employee=self._employee.id,
            back_device=self._back_device.id,
            date='2015-01-01',
            start_time='01:00',
            end_time='02:00',
        )


        shift_winter = Shift.build_from_dictionary(
            request_body = request_body,
            department = self._department,
            company = self._company
        )

        request_body['date'] = '2015-06-06'

        shift_summer = Shift.build_from_dictionary(
            request_body = request_body,
            department = self._department,
            company = self._company
        )

        self.assertNotEqual(
            shift_winter.start_time.astimezone(pytz.utc).hour,
            shift_summer.start_time.astimezone(pytz.utc).hour)
        self.assertNotEqual(
            shift_winter.end_time.astimezone(pytz.utc).hour,
            shift_summer.end_time.astimezone(pytz.utc).hour)


        sw = shift_winter.to_dict()
        ss = shift_summer.to_dict()

        self.assertEqual(sw['start_time'], ss['start_time'])
        self.assertEqual(sw['end_time'], ss['end_time'])

