# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0002_auto_20150702_1512'),
    ]

    operations = [
        migrations.AddField(
            model_name='employee',
            name='notes',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='employee',
            name='yrs_old',
            field=models.IntegerField(null=True),
        ),
    ]
