# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LiftsView',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('employee_id', models.CharField(max_length=255)),
                ('shift_id', models.CharField(max_length=255)),
                ('shift_date', models.CharField(max_length=255)),
                ('device_name', models.CharField(max_length=255)),
                ('this_timestamp', models.DateTimeField()),
                ('back_angle', models.FloatField()),
            ],
            options={
                'db_table': 'v_lifts',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, primary_key=True)),
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255)),
                ('street1', models.CharField(max_length=255, null=True)),
                ('street2', models.CharField(max_length=255, null=True)),
                ('city', models.CharField(max_length=255, null=True)),
                ('state', models.CharField(max_length=2, null=True)),
                ('timezone', models.CharField(default=b'America/New_York', max_length=100, choices=[(b'US/Eastern', b'US/Eastern'), (b'US/East-Indiana', b'US/East-Indiana'), (b'US/Michigan', b'US/Michigan'), (b'US/Central', b'US/Central'), (b'US/Indiana-Starke', b'US/Indiana-Starke'), (b'US/Mountain', b'US/Mountain'), (b'US/Pacific', b'US/Pacific'), (b'US/Hawaii', b'US/Hawaii')])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CompanyHasDevice',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, primary_key=True)),
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('collection_time', models.DateTimeField()),
                ('release_time', models.DateTimeField(null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CompanyHasUser',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, primary_key=True)),
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('company', models.ForeignKey(to='dashboard.Company')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, primary_key=True)),
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255)),
                ('company', models.ForeignKey(to='dashboard.Company')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Device',
            fields=[
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('id', models.CharField(max_length=32, serialize=False, primary_key=True)),
                ('series', models.CharField(max_length=7)),
                ('number', models.CharField(max_length=4)),
                ('label', models.CharField(max_length=255)),
                ('wrist', models.BooleanField(default=False)),
                ('notes', models.CharField(max_length=255, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, primary_key=True)),
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('given_name', models.CharField(max_length=255)),
                ('family_name', models.CharField(max_length=255, null=True)),
                ('height', models.FloatField(null=True)),
                ('weight', models.FloatField(null=True)),
                ('dob', models.DateField(null=True)),
                ('gender', models.CharField(default=b'M', max_length=1, choices=[(b'M', b'Male'), (b'F', b'Female')])),
                ('phone', models.CharField(max_length=100, null=True)),
                ('left_handed', models.BooleanField(default=False)),
                ('company', models.ForeignKey(to='dashboard.Company')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, primary_key=True)),
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255)),
                ('company', models.ForeignKey(to='dashboard.Company')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Shift',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, serialize=False, primary_key=True)),
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('start_time', models.DateTimeField()),
                ('end_time', models.DateTimeField()),
                ('notes', models.CharField(max_length=255)),
                ('processed_on', models.DateTimeField(null=True)),
                ('back_device', models.ForeignKey(related_name='+', to='dashboard.Device')),
                ('department', models.ForeignKey(to='dashboard.Department')),
                ('employee', models.ForeignKey(to='dashboard.Employee')),
                ('job', models.ForeignKey(to='dashboard.Job', null=True)),
                ('wrist_device', models.ForeignKey(related_name='+', to='dashboard.Device', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
