# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='companyhasuser',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='companyhasdevice',
            name='company',
            field=models.ForeignKey(to='dashboard.Company'),
        ),
        migrations.AddField(
            model_name='companyhasdevice',
            name='device',
            field=models.ForeignKey(to='dashboard.Device'),
        ),
    ]
