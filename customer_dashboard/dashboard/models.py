import uuid
import re
from customer_dashboard.landing.models import User
from customer_dashboard.helpers import *
from customer_dashboard.settings import BAD_LIFTS_ANGLE as bad_lifts_angle
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from datetime import datetime, date, timedelta
import time
from metrics_models import *
from django.db import IntegrityError

from django.db.models import Q

class Metrics(object):
            start_date_extended = None
            start_date = None
            end_date = None
            _metrics = None
            _days_with_shifts = None

            @property
            def metrics(self):
                return list(self._metrics)

            @metrics.setter
            def metrics(self, queryset):
                self._metrics = queryset

            @property
            def days_with_shifts(self):
                return ["%s-%02d-%02d" % (s['year'], s['month'], s['day']) for s in self._days_with_shifts]

            @days_with_shifts.setter
            def days_with_shifts(self, queryset):
                self._days_with_shifts = queryset

            def __init__(self, start_date_extended, start_date, end_date, metrics, days_with_shifts):
                self.start_date_extended = start_date_extended
                self.start_date = start_date
                self.end_date = end_date
                self.metrics = metrics
                self.days_with_shifts = days_with_shifts


class AppManager(Manager):
    def get_queryset(self):
        return super(AppManager, self).get_queryset().using('default').filter(active=True)


class BaseUUID(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=True)

    class Meta:
        abstract = True


class BaseFunctional(models.Model):
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True)

    objects = AppManager()

    @classmethod
    def validate_input(cls, input_dict):
        """
        Matches the input dictionary with the fields that the class has
        :param input_dict: dict with the keys as class attributes and values
        :return: dictionary with the keys to be the valid attributes of the class
        """
        output_dict = dict()
        for field in cls._meta.fields:
            field_value = input_dict.get(field.name, None)
            if field_value:
                output_dict[field.name] = field_value

        company_id = input_dict.get('company_id', None)
        if company_id is not None:
            output_dict['company'] = Company.objects.get(pk=company_id)

        return output_dict

    def delete(self, hard_delete=False):
        """
        Hard delete - removes the object from the database
        :param hard_delete: Boolean
        :return: void
        """
        if hard_delete:
            super(BaseFunctional, self).delete()
        else:
            self.active = False
            self.save()

    def to_dict(self, **kwargs):
        """
        Converts instance to a dictionary with the valid values for converting to json
        :param kwargs: supplementary information for conversion of non-json serializable objects
        :return: dict
        """
        output_dict = dict()
        for field in self._meta.fields:
            field_value = getattr(self, field.name, '')
            if isinstance(field_value, uuid.UUID):
                field_value = str(field_value)
            if isinstance(field_value, datetime):
                timezone = kwargs.get('timezone', pytz.utc)
                field_value = field_value.astimezone(timezone).strftime(kwargs.get('date_format', '%Y-%m-%d'))
            if isinstance(field_value, date):
                field_value = field_value.strftime(kwargs.get('date_format', '%Y-%m-%d'))
            if isinstance(field_value, BaseFunctional):
                field_value = str(field_value.id)
            output_dict[field.name] = field_value
        return output_dict

    @classmethod
    def build_from_dictionary(cls, request_body):
        """
        Finds the object OR creates a new one
        :param request_body: dict
        :return:
        """

        object_id = getattr(cls, 'id', None) or request_body.get('id', None)

        request_body.pop('id', None)
        validated_fields = cls.validate_input(request_body)

        if object_id:
            obj = cls.objects.get(pk=object_id)
            obj.update_from_dictionary(validated_fields)
        else:
            obj = cls.objects.create(**validated_fields)

        return obj

    def update_from_dictionary(self, dictionary):
        """
        Updates instance values with the new ones from the dictionary
        :param dictionary:
        :return:
        """
        for key, value in dictionary.iteritems():
            if not key == 'id':
                setattr(self, key, value)
        self.save()

    class Meta:
        abstract = True


class Base(BaseUUID, BaseFunctional):
    pass

    class Meta:
        abstract = True


class Company(Base):
    TIMEZONES = get_timezones()

    name = models.CharField(max_length=255)
    street1 = models.CharField(max_length=255, null=True)
    street2 = models.CharField(max_length=255, null=True)
    city = models.CharField(max_length=255, null=True)
    state = models.CharField(max_length=2, null=True)  # assuming there's only US market now
    timezone = models.CharField(max_length=100, choices=TIMEZONES, default='America/New_York')

    @classmethod
    def build_from_request(cls, request_body, request_user=None):

        """
        Creates Company object from request_body and request_user
        :param request_body: dict
        :param request_user: User from HttpRequest object
        :return: instance of Company Object
        """

        company = cls.build_from_dictionary(request_body)
        if not request_body.get('id', None):
            CompanyHasUser.objects.create(company=company, user=request_user)

        return company

    def get_metrics(self, start_day, end_day):

        end_day = end_day or start_day
        company_tz = pytz.timezone(self.timezone)

        if end_day:
            end_day = datetime.strptime(end_day, '%Y-%m-%d')
            end_day = company_tz.localize(end_day)
        else:
            end_day = datetime.now(tz=company_tz)

        end_date = end_day.replace(hour=23, minute=59, second=59, microsecond=999999)

        if start_day:
            start_day = datetime.strptime(start_day, '%Y-%m-%d')
            start_day = company_tz.localize(start_day)
            delta = start_day - end_date
            start_date = start_day + delta  # + -delta_of_days
        else:
            start_date = end_date - timedelta(days=14)

        start_date = start_date.replace(hour=0, minute=0, second=0, microsecond=0)
        start_date_extended = start_date

        # in case if the user has picked less than the week range
        if (end_date - start_date).days < 14:
            start_date_extended = start_date - timedelta(days=(14 - (end_date - start_date).days))

        # in db we store epoch timestamp with milliseconds, so the result needs to be multiplied to 1000
        shift_end = int(time.mktime(end_date.timetuple())) * 1000 + 999
        shift_start = int(time.mktime(start_date_extended.timetuple())) * 1000

        metrics = LiftsView.objects.filter(
            Q(company_id=str(self.id)),
            (Q(back_angle__gte=bad_lifts_angle) | Q(back_angle__isnull=True)),
            Q(shift_start__gte=shift_start),
            Q(shift_end__lte=shift_end)
            ).order_by('job_name',
                       'shift_start_year',
                       'shift_start_month',
                       'shift_start_day')

        days_with_shifts = LiftsView.objects\
                                    .filter(company_id=str(self.id))\
                                    .extra(select={'year': 'shift_start_year',
                                                   'month': 'shift_start_month',
                                                   'day': 'shift_start_day'})\
                                    .values('year', 'month', 'day')\
                                    .order_by('year', 'month', 'day')\
                                    .distinct()

        return Metrics(start_date_extended, start_date, end_date, metrics, days_with_shifts)

    @staticmethod
    def process_metrics(metrics, company_tz):

        output_dict = {}
        for m in metrics:
            valuable_metrics = ValuableMetrics(m)
            valuable_metrics = valuable_metrics.to_dict(company_tz)
            employee_id = valuable_metrics['employee_id']

            employee_meta = dict(lifts=[],
                                 name=valuable_metrics.get('employee_name'),
                                 true_length=valuable_metrics.get('true_length'),
                                 shift_delta=valuable_metrics.get('shift_delta'))

            job_name = m['job_name']

            shift_date = datetime(
                year=m['shift_start_year'],
                month=m['shift_start_month'],
                day=m['shift_start_day']).strftime('%Y-%m-%d')

            if job_name not in output_dict:
                output_dict[job_name] = dict()

            if shift_date not in output_dict[job_name]:
                output_dict[job_name][shift_date] = dict()

            if employee_id not in output_dict[job_name][shift_date]:
                output_dict[job_name][shift_date][employee_id] = employee_meta

            if valuable_metrics.get('lift_end'):
                output_dict[job_name][shift_date][employee_id]['lifts'].append(valuable_metrics.get('lift_end'))

        return output_dict

    @classmethod
    def build_from_dictionary(cls, request_body):

        """
        Overriden method of the Base class
        Receives both: company options save request and company object + company itself save call
        Will be, probably, used as emergency save when user closes the tab or reloads the browser tab

        :param request_body: dict
        :return: company settings
        """
        company_settings = request_body.get('settings', None)

        if company_settings:
            request_body.pop('settings')
            object_mapping = {
                'departments': Department,
                'devices': Device,
                'employees': Employee,
                'jobs': Job
            }
            for objects in request_body:
                obj_list = [dict(obj, **{'company_id': company_settings['id']}) for obj in
                            request_body.get(objects, [])]
                obj_class = object_mapping[objects]
                for obj in obj_list:
                    obj_class.build_from_dictionary(obj)
        else:
            company_settings = request_body

        return super(Company, cls).build_from_dictionary(company_settings)

    def to_dict(self, **kwargs):
        output_dict = super(Company, self).to_dict(**kwargs)
        devices = getattr(self, 'devices', None)
        if devices:
            output_dict['devices'] = [device.to_dict() for device in devices]
        return output_dict

    def __str__(self):
        return '<%s name:%s id:%s>' % (self.__class__.__name__, self.name, self.id)

    @classmethod
    def get_extended_object(cls, user, company_id):

        """
        Returns the company with all dependable objects: employees, departments and devices
        :param user: user from request object
        :param company_id: [optional] - company id, in case if the user owns or is in multiple companies
        :return:dict(company={extended company object}, user_companies={all companies that are connected with the user})
        """

        user_companies = CompanyHasUser.objects.filter(user=user)
        extended_object = None
        company = None

        if company_id:
            company = cls.objects.get(pk=company_id)
        elif user_companies:
            company = user_companies[0].company

        if company:
            extended_object = {
                'settings': company.to_dict(),
                'departments': [obj.to_dict() for obj in Department.objects.filter(company=company)],
                'employees': [obj.to_dict() for obj in Employee.objects.filter(company=company)],
                'devices': [obj.to_dict() for obj in CompanyHasDevice.objects.filter(company=company)],
                'jobs': [obj.to_dict() for obj in Job.objects.filter(company=company)]
            }

        return dict(company=extended_object, user_companies=[obj.to_dict() for obj in user_companies])


class CompanyHasUser(Base):
    company = models.ForeignKey(Company)
    user = models.ForeignKey(User)

    def to_dict(self):
        return dict(id=str(self.company.id), name=self.company.name)


class Department(Base):
    company = models.ForeignKey(Company)
    name = models.CharField(max_length=255)

    def to_dict(self, **kwargs):
        output_dict = super(Department, self).to_dict()
        output_dict['timezone'] = self.company.timezone
        return output_dict

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if len(Department.objects.filter(company=self.company, name=self.name)) > 1:
            raise IntegrityError('Two department with the same name are prohibited')

        super(Department, self).save(force_insert, force_update, using, update_fields)

    @classmethod
    def get_bound_company(cls, user, department_id, with_metrics=False, start_day=None, end_day=None):
        department = cls.objects.get(pk=department_id)
        context = Company.get_extended_object(user, str(department.company.id))
        context['department'] = department.to_dict()
        if with_metrics:
            metrics = department.company.get_metrics(start_day=start_day, end_day=end_day)

            company_tz = pytz.timezone(department.company.timezone)
            context['company_metrics'] = dict(data=Company.process_metrics(metrics.metrics, company_tz=company_tz),
                                              days_with_shifts=metrics.days_with_shifts,
                                              start_date=metrics.start_date.strftime("%Y-%m-%d"),
                                              end_date=metrics.end_date.strftime("%Y-%m-%d"))
        return context

    def get_schedule(self, department, schedule_date):

        date_pattern = re.compile("^(20)\d\d\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$")
        if date_pattern.match(schedule_date):

            shift_start = datetime.strptime("%s 00:00:00" % schedule_date, '%Y-%m-%d %H:%M:%S')
            shift_end = datetime.strptime("%s 23:59:59" % schedule_date, '%Y-%m-%d %H:%M:%S')

            company_tz = pytz.timezone(self.company.timezone)

            schedule = Shift.objects.filter(department=department,
                                            start_time__gte=company_tz.localize(shift_start),
                                            start_time__lte=company_tz.localize(shift_end))

            return dict(schedule=dict(items=[shift.to_dict() for shift in schedule]))

        else:
            return dict(msg='Incorrect date %s' % schedule_date), 400

    def create_shift(self, request_body, company, department):

        shift = Shift.build_from_dictionary(request_body, company=company, department=department)
        return shift.to_dict()

    def update_shift(self, request_body, company, department):

        shift = Shift.build_from_dictionary(request_body, company=company, department=department)
        return shift.to_dict()

    def create_schedule(self, request_body, company, department):

        shifts = []
        for shift in request_body.get('items', []):
            shift['date'] = request_body.get('date')
            shifts.append(Shift.build_from_dictionary(shift, company=company, department=department))

        return dict(items=[shift.to_dict() for shift in shifts])


class Employee(Base):
    GENDER = (
        ('M', 'Male'),
        ('F', 'Female')
    )

    company = models.ForeignKey(Company)
    given_name = models.CharField(max_length=255)
    family_name = models.CharField(max_length=255, null=True)
    height = models.FloatField(null=True)
    weight = models.FloatField(null=True)
    dob = models.DateField(null=True)
    gender = models.CharField(max_length=1, choices=GENDER, default='M')
    notes = models.CharField(max_length=255, null=True)
    yrs_old = models.IntegerField(null=True)
    left_handed = models.BooleanField(default=False)

    @property
    def age(self):
        return self.yrs_old

    @age.setter
    def age(self, value):
        try:
            self.yrs_old = int(value)
        except (ValueError, TypeError):
            self.yrs_old = None

    def get_name(self):
        if self.given_name and self.family_name:
            return "%s %s" % (self.given_name, self.family_name)
        return self.given_name if self.given_name else self.family_name

    def to_dict(self, **kwargs):
        output_dict = super(Employee, self).to_dict(**kwargs)
        output_dict['age'] = output_dict.pop('yrs_old', '')
        return output_dict


class Job(Base):
    company = models.ForeignKey(Company)
    name = models.CharField(max_length=255)

    @classmethod
    def build_from_dictionary(cls, request_body, company=None):

        job = None
        job_id = request_body.get('id', None)
        job_name = request_body.get('name', None)

        try:
            job = cls.objects.get(pk=job_id, company=company)
        except (ValueError, ObjectDoesNotExist):
            if job_name is not None:
                try:
                    job = cls.objects.get(name=job_name, company=company)
                except (ValueError, ObjectDoesNotExist):
                    job = cls.objects.create(name=job_name, company=company)
        return job

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.name = self.name.lower()
        super(Job, self).save(force_insert,force_update,using,update_fields)


class Device(BaseFunctional):
    id = models.CharField(max_length=32, primary_key=True)
    series = models.CharField(max_length=7)  # change to 2 when we'll get rid of kineticxxx serials
    number = models.CharField(max_length=4)
    label = models.CharField(max_length=255)
    wrist = models.BooleanField(default=False)
    notes = models.CharField(max_length=255, null=True)

    @property
    def name(self):
        if self.series == 'kinetic':  # TODO removeme when no devices kineticxxx will remain
            return "%s%s" % (self.series, self.number)
        return "%s-%s" % (self.series, self.number)

    @name.setter
    def name(self, value):
        name = value.split('-')
        if len(name) > 1:
            self.series = name[0]
            self.number = name[1]
        else:
            # TODO fixme: assuming, we're still dealing with kineticxxx names
            self.series = 'kinetic'
            self.number = value[-3:]

    @classmethod
    def get_series(cls):
        return cls.objects.order_by('series').distinct('series').values('series')

    @classmethod
    def build_from_dictionary(cls, request_body):
        """
        Finds the object OR creates a new one
        :param request_body: dict
        :return:
        """

        company_id = request_body.get('company_id', None)

        device_id = getattr(cls, 'id', None) or request_body.get('id', None)

        if company_id and device_id:
            company = Company.objects.get(pk=company_id)

            device = cls.objects.get(pk=device_id)
            collection_time = datetime.now()
            release_time = None
            if request_body.get('collection_time', None):
                release_time = datetime.strptime(request_body.get('collection_time'), '%Y-%m-%d %H:%M:%S')

                if collection_time < release_time:
                    release_time = None

            company_tz = pytz.timezone(company.timezone)
            many_to_many = CompanyHasDevice(
                company=company,
                device=device,
                collection_time=company_tz.localize(collection_time),
                release_time=company_tz.localize(collection_time) if collection_time else None
                # TODO work on this: the time gets saved
            )

            many_to_many.save()

            return device

        # TODO Make it look nicer!
        return False

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        pattern = re.compile('^[\d\w]{2}.*\d{4}$', re.IGNORECASE)
        legacy_pattern = re.compile('^kinetic\d{3}$')
        name = str(self.name)

        if pattern.match(name):
            new_serial = name[:2].upper()
            new_number = name[-4:]

        # legacy code to match kineticxxx names
        elif legacy_pattern.match(name):
            new_serial = 'kinetic'
            new_number = name[-3:]
        else:
            try:
                new_serial, new_number = automatically_assign_device_name(Device.objects.latest('created_at'))
            except ObjectDoesNotExist:
                new_serial, new_number = 'AA', '0001'
        self.name = "%s-%s" % (new_serial, new_number)

        return super(Device, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                        update_fields=update_fields)

    def to_dict(self, **kwargs):
        output_dict = super(Device, self).to_dict(**kwargs)
        output_dict['name'] = "%s-%s" % (output_dict['series'], output_dict['number'])

        if output_dict['series'] == 'kinetic':  # TODO removeme after we'll get rid of kineticxxx device ids
            output_dict['name'] = "%s%s" % (output_dict['series'], output_dict['number'])

        return output_dict


class CompanyHasDevice(Base):
    company = models.ForeignKey(Company)
    device = models.ForeignKey(Device)
    collection_time = models.DateTimeField()
    release_time = models.DateTimeField(null=True)

    def to_dict(self, **kwargs):
        self_output_dict = super(CompanyHasDevice, self).to_dict()
        output_dict = self.device.to_dict()
        output_dict['company_id'] = self_output_dict['company']
        output_dict['collection_time'] = self_output_dict['collection_time']
        output_dict['release_time'] = self_output_dict['release_time']
        return output_dict


class Shift(Base):
    department = models.ForeignKey(Department)
    employee = models.ForeignKey(Employee)
    job = models.ForeignKey(Job, null=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(null=True)
    back_device = models.ForeignKey(Device, related_name='+')
    wrist_device = models.ForeignKey(Device, null=True, related_name='+')
    notes = models.CharField(max_length=255)
    processed_on = models.DateTimeField(null=True)

    def to_dict(self, **kwargs):
        return super(Shift, self).to_dict(date_format='%H:%M:%S',
                                          timezone=pytz.timezone(self.department.company.timezone))

    @classmethod
    def build_from_dictionary(cls, request_body, company=None, department=None):

        try:
            shift = cls.objects.get(pk=request_body.get('id', None), department=department)
        except (ObjectDoesNotExist, ValueError):
            shift = cls(department=department)

        shift.employee = Employee.objects.get(pk=request_body.get('employee'), company=company)
        back_device = Device.objects.get(pk=request_body.get('back_device'))

        # basically checking if the company has the device
        company_device = CompanyHasDevice.objects.get(company=company, device=back_device)

        shift.back_device = company_device.device

        start_time = datetime.strptime('%s %s' % (request_body.get('date'), request_body.get('start_time')),
                                       '%Y-%m-%d %H:%M:%S')
        end_time = None
        if request_body.get('end_time'):
            end_time = datetime.strptime('%s %s' % (request_body.get('date'), request_body.get('end_time')),
                                         '%Y-%m-%d %H:%M:%S')

        if end_time and start_time > end_time:
            end_time = end_time + timedelta(days=1)

        company_tz = pytz.timezone(department.company.timezone)

        shift.start_time = company_tz.localize(start_time)

        if end_time:
            shift.end_time = company_tz.localize(end_time)

        shift.job = Job.build_from_dictionary(
            dict(id=request_body.get('job', None), name=request_body.get('job_name', None)),
            company)

        try:
            wrist_device = Device.objects.get(pk=request_body.get('wrist_device'))
        except (ValueError, ObjectDoesNotExist):
            wrist_device = None

        shift.wrist_device = wrist_device

        shift.notes = request_body.get('notes', '')

        shift.save()

        return shift

    @classmethod
    def get_shifts_between_the_dates(cls, start_date, end_date=None):
        pass
