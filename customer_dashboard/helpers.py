from datetime import datetime
from string import ascii_uppercase
import pytz

def timezone_mapper(a):
    now = datetime.now(pytz.timezone(a))
    timezone_str = now.strftime('%z')[1:]
    timezone_sign = now.strftime('%z')[:1]
    timezone_offset = datetime.strptime(timezone_str, '%H%M')
    return (a, timezone_sign, timezone_offset.hour, timezone_offset.minute)

def get_timezones():

    timezones = [
        'US/Central',
        'US/Eastern',
        'US/East-Indiana',
        'US/Hawaii',
        'US/Indiana-Starke',
        'US/Michigan',
        'US/Mountain',
        'US/Pacific',
    ]

    timezones = map(timezone_mapper, timezones)
    timezones = sorted(timezones, key=lambda a: (True if a[1] == '+' else False, a[2], a[3]))
    timezones = map(lambda a: (a[0], a[0]), timezones)

    return timezones

def automatically_assign_device_name(latest_device):

    latest_name = latest_device.name
    serial = latest_name[:2]
    number = latest_name[-4:]
    if int(number) < 9999:
        new_number = "%04d" % (int(number) + 1)
        new_serial = serial
    else:
        new_number = '0001'
        s1 = serial[0]
        s2 = serial[1]
        letters = list(ascii_uppercase)
        if s2 == letters[-1]:
            s2 = letters[0]
            try:
                s1 = letters[letters.index(s1) + 1]
            except IndexError as e:
                raise type(e)('We made this!!! We\'ve sold whole bunch of devices!')
        else:
            s2 = letters[letters.index(s2) + 1]
        new_serial = "%s%s" % (s1,s2)
    return new_serial, new_number





