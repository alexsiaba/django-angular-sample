var dashboardApp = angular.module('dashboardApp', ['ngCookies']);

dashboardApp

.factory('httpRequestInterceptor', function ($cookies) {
    return {
        request: function (config) {
            config.headers = {'X-CSRFToken': $cookies.csrftoken};
            return config;
            }
    };
})

.service('SharedData', function(){
    var data = {};
    return {
        set: function (key, value) {
            data[key] = value;
        },
        get: function (key) {
            return data[key];
        }
    }
})

.config(function ($httpProvider) {
    $httpProvider.interceptors.push('httpRequestInterceptor');
})
.directive('alert', function(){
    return {
        restrict: 'E',
        templateUrl: 'alert'
    }
})
.directive('department', function(){
    return {
        restrict: 'E',
        templateUrl: 'department'
    }
})

.directive('navbarDepartment', function(){
    return {
        restrict: 'E',
        templateUrl: 'navbar_department',
        replace: true,
    }
})

.directive('navbarCompany', function(){
    return {
        restrict: 'E',
        templateUrl: 'navbar_company',
        replace: true,
    }
})

.directive('device', function(){
    return {
        restrict: 'E',
        templateUrl: 'device'
    }
})

.directive('company', function(){
    return {
        restrict: 'E',
        templateUrl: 'company'
    }
})

.directive('allocatedDevice', function(){
    return {
        restrict: 'E',
        templateUrl: 'allocated_device'
    }
})

.directive('companyDevice', function(){
    return {
        restrict: 'E',
        templateUrl: 'company-device'
    }
})

.directive('deviceIcon', function(){
    return {
        restrict: 'E',
        templateUrl: 'device-icon'
    }
})

.directive('employee', function(){
    return {
        restrict: 'E',
        templateUrl: 'employee'
    }
})

.directive('worker', function(){
    return {
        restrict: 'E',
        templateUrl: 'worker',
    }
})
//not used
.directive('draggable', function($document) {
    return function(scope, element, attr) {
        var startX = 0, startY = 0, x = 0, y = 0;
        element.on('mousedown', function(e) {
            e.preventDefault();
            startX = e.screenX - x;
            startY = e.screenY - y;
            $document.on('mousemove', mousemove);
            $document.on('mouseup', mouseup);
        });

        function mousemove(e) {
            y = e.screenY - startY;
            x = e.screenX - startX;
            element.css({
                top: y + 'px',
                left:  x + 'px'
            });
        }

        function mouseup() {
            $document.off('mousemove', mousemove);
            $document.off('mouseup', mouseup);
        }
    };
})

.directive('calendar', function(){
    return {
        restrict: 'E',
        templateUrl: 'calendar',
        scope: {
            calendar: '='
        },
        link: function(scope) {
            return scope.calendar.init();
        }
    };
})

.filter('capitalize', function() {
    return function(input, scope) {
        if (input!=null)
        input = input.toLowerCase();
        return input.substring(0,1).toUpperCase()+input.substring(1);
    }
})

.filter('range', function() {
    return function(input, from, to, reverse) {
        if(typeof to === 'undefined'){
            to = from;
            from = 0;
        }
        from = parseInt(from)
        to = parseInt(to)
        if(to > from){
            for(;++from<=to;){
                input.push(from);
            }
        }
        return reverse ? input.reverse() : input;
    };
})

.factory('Collection', function(){

    function collection_sorting_function(a,b){
        if(a.name > b.name){
            return 1;
        }
        return -1;
    }

    function Collection(obj, args, api_name, sorting_function){
        this.items = [];
        this.obj = obj;
        this.api_name = api_name;
        if(typeof sorting_function === 'undefined'){
            sorting_function = collection_sorting_function
        }
        if(args instanceof Array){
            args.sort(sorting_function)
            for(var i in args){
                o = new obj(args[i]);
                if(o._isValid()){
                    o._collection = this;
                    this.items.push(o);
                }
            }
        };
        this.getChanged = function(){
            collection = [];
            for(var i in this.items){
                if(this.items[i]._validate){
                    collection.push(this.items[i]);
                } else {
                    console.log(this.items[i]._validation_errors)
                }
            }
            return collection;
        };

        this.find = function(id, return_idx){
            for(var i in this.items){
                if(this.items[i].id === id){
                    if(return_idx){
                        return i
                    } else {
                        return this.items[i];
                    }
                }
            }
            return null;
        }

        this.to_json_collection = function(){
            var json_collection = []
            for(var i in this.items){
                json_collection.push(this.items[i]._validate());
            }
            return json_collection;
        }

        this.save = function(company){
            company.saveObject(this.items)
        }
    }
    return Collection;
})

.factory('Calendar', function(){

    function Calendar(startDate, endDate, specialDays){

        if(startDate && endDate){
            this.startDate = endDate //the date from which the calendar will be initialized
            this.selected = startDate.clone()
            this.selected_end = endDate.clone()
            this.panes = [{},{}]
            this.selecting_start = true
        } else {
            this.startDate = moment()
            this.panes = [{}]
            this.selected = this.startDate.clone().startOf('day')
            this.selected_end = null
        }

        this.specialDays = specialDays || []
        this.month = this.selected.clone()


        this.init = function(){
            var start = this.selected.clone()
            start.startOf('month').startOf('week')
            for(var i in this.panes){
                _buildMonth(this.panes, i, start, this.month)
            }
        }

        this.select = function(day){
            if(this.panes.length === 1 || this.selecting_start){
                this.selected = day.date
                this.selected_end = this.selected.clone()
            } else {
                this.selected_end = day.date
                if(this.selected_end.isBefore(this.selected)){
                    var cached_selected = this.selected.clone()
                    this.selected = this.selected_end
                    this.selected_end = cached_selected
                }
            }
            this.selecting_start = !this.selecting_start
        }

        this.next = function(){
            var next = this.month.clone();
            next.month(next.month()+1).date(1).startOf('week')
            this.month.month(this.month.month()+1)
            for(var i in this.panes){
                _buildMonth(this.panes, i, next, this.month)
            }
        }

        this.previous = function(){
            var previous = this.month.clone()
            previous.month(previous.month()-1).date(1).startOf('week')
            this.month.month(this.month.month()-1)
            for(var i in this.panes){
                _buildMonth(this.panes, i, previous, this.month)
            }
        }

        function _buildMonth(pane, idx, start, month) {
            pane[idx].weeks = []
            var done = false
            var date = start.clone()
            var month = month.clone()
            if(idx === '1'){
                date.add(1, 'M').startOf('week')
                if(date.month() === date.clone().endOf('week').month()){
                    date.add(1, 'w')
                }
                month.add(1, 'M')
            }
            var monthIndex = date.month()
            var count = 0;
            while (!done) {
                pane[idx].weeks.push({ days: _buildWeek(date.clone(), month) })
                date.add(1, "w")
                done = count++ > 2 && monthIndex !== date.month()
                monthIndex = date.month()
            }
        }

        function _buildWeek(date, month) {
            var days = [];
            for (var i = 0; i < 7; i++) {
                days.push({
                    name: date.format("dd").substring(0, 1),
                    number: date.date(),
                    isCurrentMonth: date.month() === month.month(),
                    isToday: date.isSame(new Date(), "day"),
                    date: date
                });
                date = date.clone();
                date.add(1, "d");
            }
            return days;
        }

    }

    return Calendar
})

.factory('Schedule', function($http, SharedData){

    function Schedule(scope, company){

        this.modal = {
            'visible': false,
            'show': function(){
                this.visible = true;
            },
            'hide': function(){
                this.visible = false;
            }
        }
        this.date = scope.calendar.selected || moment();

        this.company = SharedData.get('company')
        this.employees = this.company.employees.items;
        this.jobs = this.company.jobs.items;
        this.devices = this.company.devices.items;

        this.items = [];

        this.save = function(){
            var url = '/api/v1/company/{company_id}/department/{department_id}/shift/{shift_date}/'
                      .replace('{company_id}', this.company.settings.id)
                      .replace('{department_id}', this.department_id)
                      .replace('{shift_date}', this.date.format('YYYY-MM-DD'))
            var json_collection = []
            for(var i in this.items){
                var shift = this.items[i]._validate();
                shift && json_collection.push(shift)
            }
            $http.post(url, {"items": json_collection})
                .success(function(data){
                console.log(data) //////basically does nothing
            });
        }
    }

    return Schedule;
})

.factory('alert', function(){
    return {
        "severity": "",
        "msg": "",
        "show": false,
        "init": function(data, callback){
            this.msg = data.msg
            this.severity = data.severity
            this.show = true
            setTimeout(callback, 3000)
        }
    }
})

.factory('objects', function($http){

    function extend(Child, Parent) {
        var F = function() { }
        F.prototype = Parent.prototype
        Child.prototype = new F()
        Child.prototype.constructor = Child
        Child.superclass = Parent.prototype
    }

    function AppObj(){

        this._validation_errors = [];
        this._isValid = function(){
            this._validation_errors = [];
            return this._validation_errors.length === 0;
        }
        //avoiding circular structures translated to JSON
        this._validate = function(){
            var obj = {}
            for(var i in this){
                if(i[0] !== '_'){
                    obj[i] = this[i]
                }
            }
            obj._construct = this._construct
            return obj;
        }
        this.save = function(callback){
            if(this._isValid()){
                callback && callback(this._validate())
            } else {
                console.log(this._validation_errors);
            }
        }
        this._construct = function(args, validators){
            if(!validators){
                validators = {}
            }
            for(var i in args){
                if(i in this && i[0] !== '_'){
                    var validator = validators[i]
                    this[i] = validator && validator(args[i]) || args[i];
                }
            }
        }
        this.get_name = function(){
            return this.name;
        }
    }

    function Settings(args){

        Settings.superclass.constructor.call(this, args)

        this.id = '';
        this.name = '';
        this.street1 = '';
        this.street2 = '';
        this.city = '';
        this.state = '';
        this.timezone = '';

        this._construct(args);

        this.saveButtonText = function(){
            if(this.id){
                return 'Save Company';
            }
            return 'Create Company';
        };
        this._isValid = function(){
            this._validation_errors = []
            if(this.name.length < 2){
                this._validation_errors.push({'name': 'Name should be 2 or more symbols'});
            }
            return this._validation_errors.length === 0;
        };
        this.save = function(company){
            company.save()
        }
    }
    extend(Settings, AppObj)

    function Employee(args){

        Employee.superclass.constructor.call(this, args)

        this.id = '';
        this.given_name = '';
        this.family_name = '';
        this.height = '';
        this.height_ft = '';
        this.height_in = '';
        this.weight = '';
        this.dob = '';
        this.dob_day = '';
        this.dob_month = '';
        this.dob_year = '';
        this.gender = 'M';
        this.notes = '';
        this.age = '';
        this.left_handed = false;
        this.back_device = null;
        this.wrist_device = null;

        this._construct(args);

        this.getDays = function(){
            var from = 1;
            var to = 30;
            var days = [];

            if([1,3,5,7,8,10,12].indexOf(parseInt(this.dob_month)) >= 0 ){
                to = 31;
            } else if (parseInt(this.dob_month) === 2){
                to = 28;
                if(this.dob_year && parseInt(this.dob_year) % 4 === 0 && parseInt(this.dob_year) % 400 !== 0){
                    to++;
                }
            }
            for(var i=from;i<=to;i++){
                days.push(i);
            }
            if(this.dob_day > to){
                this.dob_day = to;
            }
            return days;
        };
        this.getYears = function(){
            var years = []
            var curr_year = new Date().getFullYear();
            for(var i = curr_year-80; i < curr_year-16; i++){
                years.push(i);
            }
            return years.reverse();
        };
        this._isValid = function(){
            this._validation_errors = [];
            if(this.dob && !(this.dob_year && this.dob_month && this.dob_day)){
                var dob = this.dob.split('-');
                this.dob_year = parseInt(dob[0]);
                this.dob_month = parseInt(dob[1])
                this.dob_day = parseInt(dob[2])
            }
            if(this.height && !(this.height_ft && this.height_in)){
                this.height_ft = parseInt(this.height / 12);
                this.height_in = this.height % 12;
            }
            return this._validation_errors.length === 0;
        }
        this._validate = function(){
            this.height = 12 * parseInt(this.height_ft) + parseInt(this.height_in);
            this.height = (isNaN(this.height) ? null : this.height);
            if(this.dob_year && this.dob_month && this.dob_day){
                this.dob = this.dob_year + '-' + this.dob_month + '-' + this.dob_day;
            }
            var obj = {}
            for(var i in this){
                if(i[0] !== '_'){
                    obj[i] = this[i]
                }
            }
            obj._construct = this._construct
            return obj
        }
        this.get_name = function(){
            return this.given_name + ' ' + this.family_name;
        }
    }
    extend(Employee, AppObj)

    function Device(args){

        Device.superclass.constructor.call(this, args)

        this.id = '';
        this.name = '';
        this.label = '';
        this.wrist = false;
        this.notes = '';
        this.showNotes = false;
        this.allocated = false;
        this.ready_for_allocation = false;
        this.company_id = '';

        this.toggleNote = function(){
            this.showNotes = !this.showNotes;
        }
        this.get_type = function(){
            if(device.wrist){
                return 'wrist'
            }
            return 'back'
        }

        this.save = function(idx){
            var $this = this;
            if($this.id){ //TODO here it was this.
                var promise = $http.put('/api/v1/device/' + $this.id + '/', $this._validate())
            } else {
                var promise = $http.post('/api/v1/device/', {"items": [$this._validate()]})
            }
            promise
            .success(function(data){
                $this._construct(data);
            })
        }

        this.remove = function(idx){
            $http.delete('/api/v1/device/' + this.id)
            .success(function(data){
                console.log(data)
            });
            this._collection.items.slice(idx, 1)
        }


        this._construct(args);
    }
    extend(Device, AppObj)

    function Job(args){

        Job.superclass.constructor.call(this, args)

        this.id = '';
        this.name = '';

        this._construct(args);
    }
    extend(Job, AppObj)

    function Department(args){

        Department.superclass.constructor.call(this, args)

        this.id = '';
        this.name = '';
        this._construct(args);

    }
    extend(Department, AppObj)


    function Shift(args, scope){

        Shift.superclass.constructor.call(this, args, scope)

        this.id = ''
        this.employee = ''
        this.job = ''
        this.job_name = ''
        this.start_time = ''
        this.end_time = ''
        this.back_device = null
        this.wrist_device = null
        this.notes = ''

        this._validate = function(){
            if(this.start_time){
                obj = angular.copy(this)
                obj.start_time = obj.start_time.format('HH:mm:ss');
                obj.end_time = (obj.end_time ? obj.end_time.format('HH:mm:ss') : '');
                obj.back_device = obj.back_device.id;
                return obj;
            }
            return null;

        }

        this._construct = function(args){
            if(typeof args.back_device === 'string'){
                var date = scope.schedule.date; ////////////
                start_time = args.start_time.split(':')
                this.start_time = date.clone()
                                      .hour(parseInt(start_time[0]))
                                      .minute(parseInt(start_time[1]))
                                      .second(start_time[2])
                this.end_time = args.end_time;
                if(this.end_time){
                    end_time = args.end_time.split(':')
                    this.end_time = date.clone()
                                    .hour(parseInt(end_time[0]))
                                    .minute(parseInt(end_time[1]))
                                    .second(end_time[2])
                }

                this.back_device = scope.schedule.company.devices.find(args.back_device)/////

                delete args.start_time
                delete args.end_time
                delete args.back_device

            }
            for(var i in args){
                if(i in this && i[0] !== '_'){
                    this[i] = args[i];
                }
            };
        }

        this._construct(args)


        this.actions = (function($){
            return {
                'use': function(){
                    $.start_time = moment(new Date()).tz(scope.settings.timezone);
                    $.save()
                },
                'drop': function(){
                    $.end_time = moment(new Date()).tz(scope.settings.timezone);
                    $.save();
                },
                'clone': function(e, idx){
                    e.preventDefault()
                    scope.schedule.items.splice(idx, 0, new Shift({"back_device": $.back_device}, scope))
                },
                'change_time': false,
                'start_time': ($.start_time && $.start_time.format('HH:mm:ss')),
                'end_time': ($.end_time && $.end_time.format('HH:mm:ss')),
                'update_time': function(time){
                    var t = this[time];
                    var d = scope.schedule.date.format('YYYY-MM-DD')
                    if(/^([01]\d|2[0-4])\:[0-5]\d\:[0-5]\d$/m.test(t)){
                        var new_time = moment(d + ' ' + t).tz(scope.settings.timezone);
                        if(scope.settings.manual_tz_calibration.hours > 0){
                            new_time.add(scope.settings.manual_tz_calibration)
                        } else {
                            new_time.subtract(scope.settings.manual_tz_calibration)
                        }
                        //Avoiding calling server if time does not change
                        if(!$[time] || $[time].format() !== new_time.format()){
                            $[time] = new_time;
                            //We need times and an employee to create a schedule
                            if($.start_time && $.end_time && $.employee){
                                $.save()
                            }
                        }

                    }
                }
            }
        })(this);

        this.remove = function(){
            var url = '/api/v1/company/{company_id}/department/{department_id}/shift/{shift_id}/'
                        .replace('{company_id}', scope.schedule.company.settings.id)
                        .replace('{department_id}', scope.settings.id)
                        .replace('{shift_id}', this.id);
            $http.delete(url).success(function(data){
                console.log(data)
            });
        };

        this.save = (function($){
            return function(){
                var url = '/api/v1/company/{company_id}/department/{department_id}/shift/{shift_date}/'
                        .replace('{company_id}', scope.schedule.company.settings.id)
                        .replace('{department_id}', scope.settings.id)
                        .replace('{shift_date}', scope.schedule.date.format('YYYY-MM-DD'));
                if($.id){
                    var promise = $http.put(url, $._validate())
                } else {
                    var promise = $http.post(url, $._validate())
                }
                promise
                .success(function(data){
                    $._construct(data);
                    //before creating new shift make sure no other empty shifts with the same device id exist
                    for(var i in scope.schedule.items){
                        var shift = scope.schedule.items[i]
                        if(!shift.id && shift.back_device.id === $.back_device.id){
                            return;
                        }
                    }
                    //scope.schedule.items.push(new Shift({"back_device": $.back_device}))
                });
            }
        })(this)


        this.resetJob = function(param){
            this[param] = '';
        }
    };

    extend(Shift, AppObj)

    return {
        "Employee": Employee,
        "Settings": Settings,
        "Device": Device,
        "Department": Department,
        "Job": Job,
        "Shift": Shift
    }
})
