dashboardApp.controller('navbarController', ['$scope', '$http', 'SharedData', function($scope, $http, SharedData){

    $scope.loginModal = {
        'visible': false,
        'email': '',
        'password': '',
        'company': '',
        'show': function(e){
            e.preventDefault();
            this.visible = true
        },
        'hide': function(e){
            e.preventDefault();
            this.visible = false
        },
        'tabs': {
            'login': true,
            'signup': false
        },
        'change': function(tab){
            for(var i in this.tabs){
                this.tabs[i] = i === tab;
            }
        },
        'modalHeader': function(){
            return this.tabs.login ? 'Login' : 'Signup';
        },
        'form_errors': [],
        'validateEmail': function(){
            this.form_errors = [];
            //populate this.form_errors if needed
            return true;
        },
        'doAction': function(e){
            if(e instanceof KeyboardEvent || e instanceof MouseEvent){
                if(e.keyCode === 0 || e.keyCode === 13){
                    if(this.tabs.login){
                        return this._doLogin()
                    } else {
                        return this._doSignup()
                    }
                }
            }
        },
        'doLogout': function(e){
            e.preventDefault()
            $http.get('/logout')
            .success(function(data, status){
                if(status === 200 && data.success){
                    location.href = '/';
                } else {
                    console.log(data)
                }
            })
        },
        '_doLogin': function(){
            if(this.validateEmail() && this.password){
                $http.post('/login', {'email': this.email, password: this.password})
                .success(function(data, status, headers, config){
                    if(status === 200 && data.success){
                        location.href = '/dashboard'; //additionally check GET['to'] variable for the destination
                        //console.log(data, status, headers, config)
                    } else {
                        console.log(data.msg);
                    }
                })
                .error(function(data, status, headers, config){
                    console.log(data, status, headers, config);
                })
            } else {
                return {'errors': this.form_errors};
            }
        },
        '_doSignup': function(){
            if(this.validateEmail() && this.password && this.company){
                $http.post('/signup', {'company': this.company, 'password': this.password, 'email': this.email})
                .success(function(data, status){
                    if(status === 200 && data.success){
                        location.href = '/dashboard'; //additionally check GET['to'] field
                    }
                });
            } else {
                return {'errors': this.form_errors};
            }
        }
    };

    $scope.whichActive = function(){
        var anchors = document.querySelectorAll('ul.navbar-nav>li>a');
        for(var i in anchors){
            var pattern = new RegExp(anchors[i].href);
            if(anchors[i].href && pattern.test(location.href)){
                anchors[i].parentNode.className = "active";
                break;
            }
        }
        if(document.querySelector('ul.navbar-nav>li.active') === null){
            anchors[0].parentNode.className = "active";
        }
    };

    $scope.companies = {
        'items': [],
        'visible': false,
        'show': function(){
            this.visible = true;
        },
        'hide': function(){
            this.visible = false;
        },
        'tab_click': function(e){
            e.preventDefault();
            if(this.items.length === 0){
                alert('There are no companies assigned to you!')
            } else {
                location.href = '/dashboard/company/' + this.items[0]['id'] + '/';
            }
        }
    }

    $scope.departments = {
        'items': [],
        'visible': false,
        'show': function(){
            var company = SharedData.get('company');
            this.items = company.departments.items;
            this.visible = true;
        },
        'hide': function(){
            this.visible = false;
        },
        'tab_click': function(e){
            e.preventDefault();
            if(this.items.length === 0){
                alert('There are no departments in this company!')
            } else {
                location.href = '/dashboard/department/' + this.items[0]['id'] + '/';
            }
        }
    }
}]);