dashboardApp.controller('companyDevicesController', [
'$scope', '$http', 'Collection', 'objects',
function($scope, $http, Collection, objects){

    function Company(args){
        this.id = ''
        this.name = ''
        this.street1 = ''
        this.street2 = ''
        this.city = ''
        this.state = ''
        this.devices = [];

        this.prepare_devices = function(){
            list = []
            for(var i in this.devices.items){
                list.push({"id": this.devices.items[i].id})
            }
            return list;
        }
        this._isValid = function(){
            return true;
        };

        this._construct = function(args){
            for(var i in args){
                if(i in this && i[0] !== '_'){
                    this[i] = args[i];
                }
            }
            for(var i in this.devices){
                this.devices[i].allocated = true;
            }
            this.devices = new Collection(objects.Device, this.devices, 'device')
        }

        this._construct(args);
    }

    objects.Device.prototype.allocate = function(){
        if(this.ready_for_allocation){
            $scope.devices.allocated_devices.push(this.id)
        } else {
            var idx = $scope.devices.allocated_devices.indexOf(this.id)
            $scope.devices.allocated_devices.splice(idx, 1)
        }
    }

    Company.prototype.allocateDevices = function(){
        for(var i in $scope.devices.allocated_devices){
            var device_id = $scope.devices.allocated_devices[i]
            var device_idx = $scope.devices.back.find(device_id, true)
            if(device_idx){
                var device = $scope.devices.back.items.splice(device_idx, 1)
            } else {
                device_idx = $scope.devices.wrist.find(device_id, true)
                var device = $scope.devices.wrist.splice(device_idx, 1)
            }
            device = device[0]
            device.ready_for_allocation = false
            device.allocated = true;
            device._collection = this.devices;
            device.company_id = this.id
            this.devices.items.push(device)
        }
        $scope.devices.allocated_devices = []
        var url = '/api/v1/company/{company_id}/device/'.replace('{company_id}', this.id)
        $http.post(url, {'items': this.prepare_devices()})
        .success(function(data){
            console.log(data);
        })
    }

    objects.Device.prototype.release = function(idx){
        var $this = this;
        var url = '/api/v1/company/{company_id}/device/{device_id}/'
                    .replace('{company_id}', $this.company_id)
                    .replace('{device_id}', $this.id)
        $http.delete(url)
             .success(function(data){
            var released = $this._collection.items.splice(idx, 1);
            obj = released[0]
            obj.allocated = false;
            $scope.devices[obj.get_type()].items.unshift(obj)
        })
    }

    $scope.devices = {
        "tabs": {
            "wrist": false,
            "back": true
        },
        "changeTab": function(e){
            e.preventDefault();
            for(var i in this.tabs){
                this.tabs[i] = !this.tabs[i]
            }
        },
        "allocated_devices": [],
    }

    function filterDevices(devices_collection, excluded_devices){
        filtered_collection = []
        for(var i in devices_collection){
            if(! (devices_collection[i].id in excluded_devices)){
                filtered_collection.push(devices_collection[i]);
            }

        }
        return filtered_collection;
    }

    $scope.init = function(devices, companies){
        var companies = angular.fromJson(companies)
        $scope.companies = new Collection(Company, companies, 'company')

        var allocated_devices = {}
        for(var i in $scope.companies.items){
            for(var j in $scope.companies.items[i].devices.items){
                allocated_devices[$scope.companies.items[i].devices.items[j].id] = null;
            }
        }

        var devices = angular.fromJson(devices)
        $scope.devices.back = new Collection(objects.Device, filterDevices(devices.back, allocated_devices), 'device')
        $scope.devices.wrist = new Collection(objects.Device, filterDevices(devices.wrist, allocated_devices), 'device')
    }

}]);