dashboardApp.controller('departmentController', [
'$scope', '$http', 'SharedData', 'Calendar', 'Schedule', 'objects',
function($scope, $http, SharedData, Calendar, Schedule, objects){



    Schedule.prototype.loadSchedule = function(){
        var $this = this;
        this.date = $scope.calendar.selected;

        var url = '/api/v1/company/{company_id}/department/{department_id}/schedule/{date}/'
                    .replace('{company_id}', this.company.settings.id)
                    .replace('{department_id}', $scope.settings.id)
                    .replace('{date}', this.date.format('YYYY-MM-DD'));
        $http.get(url).success(function(data){

            console.log(data)
            $this.items = [];
            for(var i in $this.devices){
                if(!$this.devices[i].wrist){
                    var shift = {"back_device": $this.devices[i]};
                    for(var j in data.schedule.items){
                        if(data.schedule.items[j].back_device === $this.devices[i].id){
                            shift = data.schedule.items[j];
                            $this.items.push(new objects.Shift(shift, $scope))
                        }
                    }
                    //removing already assigned employees from available on current schedule
                    for(var i in $this.employees){
                        if($this.employees[i].id === shift.employee){
                            $this.employees[i].hidden = true;
                        }
                    }
                }
            }
            $this.modal.visible = true;
            $this.department_id = $scope.settings.id
        });
    }

    Schedule.prototype.employeeWasPicked = function(shift){
        for(var i in this.employees){
            if(shift._employee && this.employees[i].id === shift._employee){
                this.employees[i].hidden = true;
            }
            if(this.employees[i].id === shift.employee){
                this.employees[i].hidden = false;
            }
        }
        shift.employee = shift._employee;
    }

    function graph_config(bindto, json, axis_names, config){
        var default_config = {
            "bindto": bindto,
            "data": {
                'json': json,
                'keys': {
                    'x': 'date',
                    'value': [
                        'lifts',
                        //'previous_lifts'
                        ]
                },
                "names": axis_names,
                "colors": {
                    'lifts': '#666666',
                    //'previous_lifts': '#66ffcc'
                }
            },
            "axis": {
                "x": {
                    'type': 'timeseries',
                    'tick': {
                        'fit': true,
                        'format': '%b %e, %y'
                    }
                },
            },
            "grid": {
                "y": {
                    "show": true
                }
            },
            "tooltip": (function(data){
                return {
                    contents: function (d) {
                        var idx = d[0].index
                        var val = d[0].value
                        var delta = data[idx].delta
                        deltaStr = val;
                            if(delta){
                                var cls = 'red pull-rigth'
                                var icon = 'fa fa-sort-asc'
                                if(delta < 0){
                                    var cls = 'green pull-right'
                                    var icon = 'fa fa-sort-desc'
                                    delta *= -1
                                }
                                deltaStr += ' <i class="{cls}"><i class="{icon}"></i>{delta}%</i>'
                                            .replace('{cls}', cls)
                                            .replace('{icon}', icon)
                                            .replace('{delta}', Math.round(delta * 100))
                            }
                        return '<span class="tooltip fade top in">' + deltaStr + '</span>'
                    }
                }
            })(json),
        }
        for(var i in config){
            default_config[i] = config[i]
        }

        return default_config
    }

    $scope.metrics = {
        "data": {},
        "mergeJobsToAll": function(){

            if(Object.keys(this.data).length === 0){
                this.data['all'] = {}
            } else {
                for(var job in this.data){
                    if(!('all' in this.data)){
                        this.data['all'] = angular.copy(this.data[job])
                    } else {
                        for(var date in this.data[job]){
                            if(!(date in this.data['all'])){
                                this.data['all'][date] = {}
                            }
                            for(var e_id in this.data[job][date]){
                                var employee = this.data[job][date][e_id];
                                if(!(e_id in this.data['all'][date])){
                                    this.data['all'][date][e_id] = employee
                                } else {
                                    for(var i in employee){
                                        if(i === 'shift_delta' || i === 'true_length'){
                                            this.data['all'][date][e_id][i] += employee[i]
                                        }
                                        if(i === 'lifts'){
                                            var total_employee_lifts = this.data['all'][date][e_id][i].concat(employee[i])
                                            total_employee_lifts.sort(function(a, b){
                                                return a - b
                                            })
                                            this.data['all'][date][e_id][i] = total_employee_lifts
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "start_day": moment(),
        "end_day": moment(),
        "dropdown_calendar": false,
        "get_date": function(day, format){
            return this[day].format(format ? format : 'MMMM Do')
        },
        "get_calendar_range": function(divider){
            var string = '{start_date}{divider}{end_date}'
            var start = this.calendar.selected.format('MMMM Do')
            if(!this.calendar.selected_end || this.calendar.selected.isSame(this.calendar.selected_end)){
                string = '{start_date}'
            }

            return string
                    .replace('{divider}', (divider ? divider: '/'))
                    .replace('{start_date}', start)
                    .replace('{end_date}', this.calendar.selected_end && this.calendar.selected_end.format('MMMM Do'))

        },
        "change_week": function(){
            var start = this.calendar.selected
            var end = this.calendar.selected_end

            var url = '/dashboard/department/{department_id}/{date_modifier}'

            var date_modifier = '{start_day}/'.replace('{start_day}', start.format('YYYY-MM-DD'))
            if(end && start.isBefore(end)){
                date_modifier += '{end_day}/'.replace('{end_day}', end.format('YYYY-MM-DD'))
            }

            location.href = url.replace('{department_id}', $scope.settings.id)
                               .replace('{date_modifier}', date_modifier)
        },
        "pick_week": function(e){
            e.preventDefault()
            this.dropdown_calendar = !this.dropdown_calendar
        },
        "stats": {
            "1_weekly": {
                "title": "High-Risk Lifts",
                "description_1": "Weekly Avg.",
                "description_2": "Per Worker",
                "value": 0,
                "get_value": function(){
                    return this.value
                },
                "get_class": function(){
                    return ''
                }
            },
            "2_weekly_change": {
                "title": "% Change",
                "description_1": "Previous",
                "description_2": "Week",
                "value": 0,
                "get_value": function(){
                    if(typeof this.value === 'number'){
                        return (this.value > 0 ? this.value : -this.value) + '%'
                    }
                    return this.value
                },
                "get_class": function(){
                    if(typeof this.value === 'number' && this.value > 0){
                        return 'red '
                    }
                    if(typeof this.value === 'number' && this.value < 0){
                        return 'green '
                    }
                },
                "get_arrow": function(){
                    if(typeof this.value === 'number' && this.value > 0){
                        return 'red fa-long-arrow-up'
                    }
                    if(typeof this.value === 'number' && this.value < 0){
                        return 'green fa-long-arrow-down'
                    }
                }
            },
            "3_daily": {
                "title": "High-Risk Lifts",
                "description_1": "Daily Avg.",
                "description_2": "Per Worker",
                "value": 0,
                "get_value": function(){
                    return this.value
                },
                "get_class": function(){
                    return ''
                },
            },
            "4_daily_change": {
                "title": "% Change",
                "description_1": "Previous",
                "description_2": "Day",
                "value": 0,
                "get_value": function(){
                    if(typeof this.value === 'number'){
                        return (this.value > 0 ? this.value : -this.value) + '%'
                    }
                    return this.value
                },
                "get_class": function(){
                    if(typeof this.value === 'number' && this.value > 0){
                        return 'red '
                    }
                    if(typeof this.value === 'number' && this.value < 0){
                        return 'green '
                    }
                },
                "get_arrow": function(){
                    if(typeof this.value === 'number' && this.value > 0){
                        return 'red fa-long-arrow-up'
                    }
                    if(typeof this.value === 'number' && this.value < 0){
                        return 'green fa-long-arrow-down'
                    }
                }
            }
        },
        "tabs": {
            "tabs": {
                "all": true,
            },
            "changeTab": function(e, tab){
                e.preventDefault()
                for(var i in this.tabs){
                    this.tabs[i] = i === tab
                }
                $scope.metrics.leaderboards.change_board(tab)
            }
        },
        "leaderboards": {
            "jobs": {},
            "_get_sorted_board": function(board){
                if(!board){ return []}
                var ret_board = []
                for(var i in board){
                    var worker = board[i]
                    ret_board.push({
                        'id': i,
                        'name': worker.name,
                        'ratio': worker.shift_delta / worker.true_length,
                        'lifts': Math.round(worker.lifts.length / worker.true_length * worker.shift_delta),
                        'lifts_array': worker.lifts,
                        'lifts_change': null,
                        'rank': null,
                        'rank_change': null,
                    })
                }
                return ret_board.sort(function(a,b){return a.lifts - b.lifts})
            },
            "_find_worker": function(arr, worker_id){
                for(var i in arr){
                    if(arr[i].id === worker_id){
                        return {
                            "idx": i,
                            "worker": arr[i]
                        }
                    }
                }
            },
            "change_board": function(job){
                var data = $scope.metrics.data[job]
                var curr = this._get_sorted_board(data[this.current_date])

                var dates = Object.keys(data).sort(function(a,b){
                    return new Date(a) > new Date(b)
                })

                var date_idx = dates.indexOf(this.current_date)
                var previous_board = data[dates[date_idx - 1]] //object
                var prev = this._get_sorted_board(previous_board) //array


                for(var i in curr){

                    var worker = curr[i]
                    worker.rank = parseInt(i) + 1

                    var idx_cached = date_idx
                    var previous_board_cached = angular.copy(previous_board)
                    var prev_cached = angular.copy(prev)

                    while(idx_cached > 0){
                        if(worker.id in previous_board_cached){
                            var prev_worker = this._find_worker(prev_cached, worker.id)
                            worker.lifts_change = worker.lifts - prev_cached[prev_worker.idx].lifts
                            worker.rank_change = -worker.rank + (parseInt(prev_worker.idx) + 1)
                            break
                        }
                        previous_board_cached = data[dates[--idx_cached]]
                        prev_cached = this._get_sorted_board(previous_board_cached) //optimize this
                    }
                }
                this['jobs'][job]['board'] = curr
            }
        },
        "_lifts": {}, //here we'll store cached values
        "charts": {},
        "workers": {},
        "sorted_workers": [],
        "sorted_workers_outsiders_first": [],
        "_sort_workers": function(){
            var sorted_workers = []
            for(var i in this.workers){
                var worker = this.workers[i];
                worker['id'] = i;
                worker['average_lifts'] = Math.round(worker.lifts / (Object.keys(worker.shifts).length || 1) ) //no NaN
                sorted_workers.push(worker)
            }
            sorted_workers.sort(function(a,b){return a.average_lifts - b.average_lifts});
            var chunk = []
            //var zero_lifts_workers = []
            for(var i in sorted_workers){
                //console.log(sorted_workers[i])
                //if(sorted_workers[i].lifts === 0){
                //    zero_lifts_workers.push(sorted_workers[i])
                //} else {
                    chunk.push(sorted_workers[i])
                //}
                if(chunk.length === 3){
                    this.sorted_workers.push(chunk);
                    chunk = []
                }
            }
            if(chunk.length){
                if(this.sorted_workers.length){
                    var last_pushed = this.sorted_workers[this.sorted_workers.length - 1]
                    for(var i=last_pushed.length; i >= chunk.length; i--){
                        chunk = chunk.concat(last_pushed.splice(i - 1, 1))
                    }
                }
                chunk.sort(function(a,b){return b.average_lifts-a.average_lifts})
                this.sorted_workers.push(chunk)
                chunk = []
            }
            /* lets just forget about this for a while
            if(zero_lifts_workers.length && this.sorted_workers.length > 2){
                console.log(this.sorted_workers)
                //removing the pre-last element (the last element will later become the first in the outsiders array)
                var last_line = this.sorted_workers.splice(this.sorted_workers.length - 2, 1)
                console.log(last_line)
                console.log(this.sorted_workers)
                while(zero_lifts_workers.length){
                    if(last_line.length === 3){
                        this.sorted_workers.splice(this.sorted_workers.length - 2, 0, last_line)
                        last_line = []
                    }
                    last_line.push(zero_lifts_workers.pop())
                }
            }
            */

            this.sorted_workers_outsiders_first = angular.copy(this.sorted_workers)
            this.sorted_workers_outsiders_first = this.sorted_workers_outsiders_first
                                                    .splice(this.sorted_workers_outsiders_first.length - 1)
                                                    .concat(this.sorted_workers_outsiders_first)
            if(this.sorted_workers_outsiders_first.length){
                this.sorted_workers_outsiders_first[0].sort(function(a,b){return b.average_lifts - a.average_lifts})
            }
        },
        "process_worker": function(id, worker, date, job, current_period){
            if(! this.workers.hasOwnProperty(id)){
                var period = this.get_period()
                this.workers[id] = {
                    'name': worker.name,
                    'shifts': {},
                    'lifts': 0,
                    'job': job,
                    'previous_lifts': 0,
                    'graph_built': false,
                    'graph_date': null,
                    'graph_is_visible': false,
                    'toggle_graph': function(e){
                        e && e.preventDefault()
                        var $this = this
                        $this.graph_is_visible = !$this.graph_is_visible
                        $scope.$on('open_worker_graph', function(){
                            $this.graph_is_visible = false
                        })
                    },
                    '_get_graph_config': function(){
                        var json = []
                        this.shift_lifts_to_display = 0
                        console.log(this.graph_date)
                        if(this.graph_date.format('YYYY-MM-DD') in this.shifts){
                            var shift = this.shifts[this.graph_date.format('YYYY-MM-DD')]
                            var hourly_lifts = {}
                            var range = []
                            for(var i in shift.lifts){
                                var hour = shift.lifts[i].toString()
                                if(hour in hourly_lifts){
                                    hourly_lifts[hour]++
                                } else {
                                    hourly_lifts[hour] = 1
                                }
                            }

                            for(var i in hourly_lifts){
                                hourly_lifts[i] *= shift.shift_delta / shift.true_length
                                this.shift_lifts_to_display += Math.round(hourly_lifts[i])
                                range.push(parseInt(i))
                            }

                            //adding two hours before and after and filling the gaps
                            if(range.length){
                                var start = range[0]
                                var end = range[range.length - 1]
                                for(var i=1; i <= 2; i++){
                                    range.unshift(start - i)
                                    range.push(end + i)
                                }

                                end = range[range.length - 1]
                                start = (range[0] > end ? range[0] - 24 : range[0])
                                range = []
                                for(var i=start; i<=end; i++){
                                    if(i > 23){
                                        range.push(i - 24)
                                    }else if(i < 0){
                                        range.push(24 + i)
                                    } else {
                                        range.push(i)
                                    }
                                }
                            }
                            var previous_lifts = null
                            for(var i in range){
                                var lifts = null;
                                var delta = null;
                                var h = range[i].toString()
                                if(h in hourly_lifts){
                                    lifts = Math.round(hourly_lifts[h])
                                }
                                if(lifts !== null && previous_lifts !== null){
                                    delta = (lifts - previous_lifts) / previous_lifts
                                }
                                json.push({
                                    'date': this.graph_date.hour(h).format('ha'),
                                    'lifts': lifts === null ? 0 : lifts,
                                    'previous_lifts': (lifts !== null ? previous_lifts : null),
                                    'delta': delta
                                })
                                if(lifts !== null){
                                    previous_lifts = lifts
                                }
                            }
                        }
                        return graph_config('#worker' + id,
                                            json,
                                            {'lifts': 'High-Risk Lifts for ' + worker.name},
                                            {"size": {'height': 300,"width": 600},"axis": {"x": {'type': 'category'}}})
                    },
                    'build_graph': function(e, forward){
                        e && e.preventDefault()
                        if(!this.graph_date){
                            this.graph_date = $scope.metrics.shift_day.clone()
                        }
                        if(forward === true){
                            this.graph_date.add(1, 'd')
                        }
                        if(forward === false){
                            this.graph_date.subtract(1, 'd')
                        }
                        if(! this.graph_is_visible){
                            $scope.$broadcast('open_worker_graph')
                            this.toggle_graph()
                        }
                        c3.generate(this._get_graph_config());
                    }
                }
            }
            //if current period, add the value to the workers' lifts, else add to previous lifts
            worker_lifts = Math.round(worker.lifts.length / worker.true_length * worker.shift_delta)

            this.workers[id][current_period ? 'lifts' : 'previous_lifts'] += worker_lifts
            if(current_period){
                this.workers[id]['shifts'][date] = {'lifts': worker.lifts.sort(function(a,b){return a-b}),
                                                'shift_delta': worker.shift_delta,
                                                'true_length': worker.true_length}
            }


        },
        "build_charts": function(){
            for(var i in this.charts){
                this.charts[i] = c3.generate(this.charts[i])
            }
        },

        /* Haytham special order */
        "team_members": [],
        "render_team_members": function(e, forward){
            e && e.preventDefault()
            this.team_members = [];
            if(forward === true){
                this.shift_day.add(1, 'd')
            }
            if(forward === false){
                this.shift_day.subtract(1, 'd')
            }
            var team_members = angular.copy(this.workers)
            for(var i in team_members){
                var worker = team_members[i]
                if(worker.shifts.hasOwnProperty(this.shift_day.format('YYYY-MM-DD'))){
                    var shift = worker.shifts[this.shift_day.format('YYYY-MM-DD')]
                    worker.lifts_to_display = Math.round(shift.lifts.length / shift.true_length * shift.shift_delta)
                    this.team_members.push(worker)
                }
            }

            this.team_members.sort(function(a,b){return b.lifts_to_display - a.lifts_to_display})
            if(this.team_members.length > 4){
                for(var i=0; i<3; i++){
                    var el = this.team_members.splice(this.team_members.length - 1)
                    this.team_members.splice(3 + i, 0, el[0])
                }
            }

            var team_members_sorted = []
            while(this.team_members.length){
                team_members_sorted.push(this.team_members.splice(0,3))
            }
            this.team_members = team_members_sorted


        },
        /* end Haytham special order */


        "prepare_charts_and_leaderboards": function(){
            var jobs = this.get_jobs()
            for(var i in jobs){

                //get processed data for the chart
                this.charts[jobs[i]] = this._get_data_for_chart(jobs[i])

                //assign period end date as a current date for the leaderboard
                this.leaderboards["jobs"][jobs[i]] = {}
                this.leaderboards["current_date"] = this.graph_end_day.clone().subtract(1, 'd').format('YYYY-MM-DD')
                this.leaderboards["get_current_date"] = function(){return moment(this.current_date).format('MMMM Do YYYY')}

                // ng-hide all tabs except 'all'
                if(! this.tabs.tabs.hasOwnProperty(jobs[i])){
                    this.tabs.tabs[jobs[i]] = false;
                }
                this.leaderboards.change_board(jobs[i])
            }

            // assign array of dates for the leaderboard dropdown
            this.leaderboards['period'] = this.get_period()

            this.shift_day = this.end_day.clone().subtract(1, 'd')

            this._calculate_stats()
            this._sort_workers()
            this.render_team_members()
        },
        "_get_data_for_chart": function(job){
            var data = this.get_daily_lifts_for_job(job)
            var json = []

            // merge all the stats data into one object
            var previous_lifts = angular.copy(data['current'])
            for(var i in data['current']){
                previous_lifts[i] = data['current'][i]
            }
            var prev_day = null
            var last_period_valuable_lifts = null
            for(var i in previous_lifts){
                var delta = null

                if(prev_day){
                    var lifts = (previous_lifts[i].lifts ? previous_lifts[i].lifts / previous_lifts[i].workers : 0);
                    var prev_lifts = (previous_lifts[prev_day].lifts ? previous_lifts[prev_day].lifts / previous_lifts[prev_day].workers : 0);

                    if(lifts && last_period_valuable_lifts){
                        delta = (lifts - last_period_valuable_lifts) / last_period_valuable_lifts;
                    }
                    if(lifts){
                        last_period_valuable_lifts = lifts;
                    }

                    moment(i).isAfter(this.graph_start_day.clone().subtract(1, 'd')) &&
                    moment(i).isBefore(this.graph_end_day, 'hour') &&
                    json.push({
                        'date': i,
                        'lifts': Math.round(lifts),
                        'previous_lifts': Math.round(prev_lifts),
                        'delta': delta,
                    })
                }
                prev_day = i
            }
            return graph_config('#' + job, json, {'lifts': 'High risk lifts for ' + (job === 'all' ? job : job + 's')})
        },
        "get_jobs": function(){
            var jobs = []
            for(var i in this.data){
                jobs.push(i)
            }
            jobs.splice(jobs.indexOf('all'))
            jobs.unshift('all')
            return jobs;
        },
        "get_period": function(previous){
            var start_day = this.curr_week_start
            var end_day = this.curr_week_end
            if(previous){
                start_day = this.prev_week_start
                end_day = this.prev_week_end
            }
            var dates = [start_day.format("YYYY-MM-DD")]
            var shuttle = start_day.clone()
            var time_difference = end_day.diff(start_day, 'd')
            for(var i=0; i < time_difference; i++){
                shuttle.add(1, 'd')
                dates.push(shuttle.format("YYYY-MM-DD"))
            }
            return dates
        },
        "get_daily_lifts_for_job": function(job){
            periods = {
                'previous': this.get_period(true),
                'current': this.get_period()
            }
            for(var p in periods){ //looping through previous and current
                // calculating aggregate values for each of the periods console.log('==========' + p + '==========')
                var output_period = {}
                for(var i in periods[p]){
                    //default values in case if there was nothing on this day
                    var lifts = null
                    var workers = 0
                    var date = periods[p][i]
                    if(date in this.data[job]){
                        lifts = 0
                        for(var j in this.data[job][date]){
                            // calculating aggregate data for each guy for each day console.log(date)
                            var worker = this.data[job][date][j];

                            // The formula
                            // lifts = actual_lifts / actual_length * shift_delta
                            var worker_lifts = worker['lifts'].length / worker['true_length'] * worker['shift_delta']
                            lifts += worker_lifts

                            // "pure" lifts from the db
                            // lifts += worker['lifts'].length

                            //if(date === '2015-05-22'){
                            //    console.log(worker.name, worker['lifts'].length, worker_lifts, worker['true_length'], worker['shift_delta'])
                            //}

                            /*
                            if(worker.name === 'Michelle'){
                                var hours = Math.floor(worker['true_length'] / 3600)
                                var minutes = Math.floor((worker['true_length'] - hours * 3600) / 60)
                                var seconds = Math.floor(((worker['true_length'] - hours * 3600) - minutes * 60) / 60)
                                console.log(date, worker['lifts'].length, worker['true_length'], hours + ':' + minutes + ':' + seconds)
                            }
                            */
                            job !== 'all' && this.process_worker(j, worker, date, job, p === 'current')
                        }
                        workers = Object.keys(this.data[job][date]).length
                    }
                    output_period[date] = {"lifts": lifts, "workers": workers}
                }
                periods[p] = output_period
            }
            this._lifts[job] = periods
            return periods
        },

        "_calculate_stats": function(){
            var curr_week_avg = 0;
            var curr_valuable_days = 0;

            var last_valuable_lifts_metrics = 0;
            var delta_of_last_valuable_lifts_metrics = 0;

            for(var i in this._lifts['all']['current']){
                var day = this._lifts['all']['current'][i]
                if(day.lifts && day.workers){
                    var day_avg = day.lifts/day.workers
                    curr_week_avg += day_avg
                    delta_of_last_valuable_lifts_metrics = (day_avg - last_valuable_lifts_metrics) / last_valuable_lifts_metrics
                    last_valuable_lifts_metrics = day_avg
                    curr_valuable_days++
                }
            }

            var prev_week_avg = 0;
            var prev_valuable_days = 0;

            for(var i in this._lifts['all']['previous']){
                var day = this._lifts['all']['previous'][i]
                if(day.lifts && day.workers){
                    prev_week_avg += day.lifts/day.workers
                    prev_valuable_days++;
                }
            }

            var curr = curr_week_avg / (curr_valuable_days || 1)
            var prev = prev_week_avg / (prev_valuable_days || 1)

            //console.log(this._lifts['all']['current'][Object.keys(this._lifts['all']['current'])]])

            var dates_current = Object.keys(this._lifts['all']['current'])
            var dates_previous = Object.keys(this._lifts['all']['previous'])

            this.stats['1_weekly']['value'] = Math.round(curr)
            this.stats['1_weekly']['dates'] = "The week of " + moment(dates_current[0]).format('MMMM Do') + ' - ' + moment(dates_current[dates_current.length - 1]).format('MMMM Do')

            this.stats['2_weekly_change']['value'] = Math.round((curr - prev) / prev * 100)
            this.stats['2_weekly_change']['dates'] = "Compared to " + moment(dates_previous[0]).format('MMMM Do') + ' - ' + moment(dates_previous[dates_previous.length - 1]).format('MMMM Do')

            if(this.stats['2_weekly_change']['value'] === Infinity || this.stats['2_weekly_change']['value'] === -Infinity || isNaN(this.stats['2_weekly_change']['value'])){
                this.stats['2_weekly_change']['value'] = '-'
            }
            this.stats['3_daily']['value'] = Math.round(last_valuable_lifts_metrics)
            this.stats['3_daily']['dates'] = moment(dates_current[dates_current.length - 2]).format('MMMM Do')

            if(delta_of_last_valuable_lifts_metrics === Infinity || delta_of_last_valuable_lifts_metrics === -Infinity){
                this.stats['4_daily_change']['value'] = '-'
            } else {
                this.stats['4_daily_change']['value'] = Math.round(delta_of_last_valuable_lifts_metrics * 100)
            }
            this.stats['4_daily_change']['dates'] = "Compared to " + moment(dates_current[dates_current.length - 3]).format('MMMM Do')

        }
    }

    $scope.init = function(department_id, timezone, company_metrics){
        $scope.settings.id = department_id
        $scope.settings.timezone = timezone

        var metrics = angular.fromJson(company_metrics)
        $scope.metrics.data = metrics.data

        $scope.metrics.mergeJobsToAll()

        $scope.metrics.start_day = moment.tz(new Date(metrics.start_date + " 00:00:00"), timezone)
        $scope.metrics.end_day = moment.tz(new Date(metrics.end_date + " 23:59:59"), timezone)

        $scope.metrics.curr_week_end = moment.tz(new Date(metrics.end_date + " 23:59:59"), timezone)


        $scope.metrics.curr_week_start = $scope.metrics.curr_week_end.clone()
                                                                     .subtract(6, 'd')
                                                                     .hour(0)
                                                                     .minute(0)
                                                                     .second(0)
                                                                     .millisecond(0)

        $scope.metrics.prev_week_end = $scope.metrics.curr_week_end.clone()
                                                                   .subtract(1, 'w')
        $scope.metrics.prev_week_start = $scope.metrics.curr_week_start.clone()
                                                                       .subtract(1, 'w')
        if(! metrics.user_start_day){
            metrics.user_start_day = $scope.metrics.curr_week_start.format('YYYY-MM-DD')
        }
        $scope.metrics.graph_start_day = moment.tz(new Date(metrics.user_start_day + " 00:00:00"), timezone)
        $scope.metrics.graph_end_day = moment.tz(new Date(metrics.user_end_day + " 23:59:59"), timezone)

        $scope.calendar = new Calendar(null, null, metrics.days_with_shifts)
        $scope.metrics.calendar = new Calendar($scope.metrics.graph_start_day.clone(), $scope.metrics.graph_end_day.clone(), metrics.days_with_shifts)

        var browser_tz_offset = new Date().getTimezoneOffset()
        var company_tz_offset = moment(new Date()).tz($scope.settings.timezone).format('Z')
        var company_shift = company_tz_offset.slice(0,1)
        var company_tz_hours = (company_shift === '-' ? -parseInt(company_tz_offset.slice(1,3)) : parseInt(company_tz_offset.slice(1,3)))
        var company_tz_minutes = (company_shift === '-' ? -parseInt(company_tz_offset.slice(4,6)): parseInt(company_tz_offset.slice(4,6)))

        $scope.settings.manual_tz_calibration.hours = - Math.floor(browser_tz_offset / 60) - company_tz_hours
        $scope.settings.manual_tz_calibration.minutes = - (browser_tz_offset % 60) - company_tz_minutes

        $scope.metrics.prepare_charts_and_leaderboards()

        angular.element(document).ready(function(){$scope.metrics.build_charts()});
    }

    $scope.settings = {
        "id": null,
        "timezone": null,
        "manual_tz_calibration":{
            "hours": 0,
            "minutes": 0
        }
    }

    $scope.tabs = {
        "tabs": {
            "dashboard": true,
            "leaderboard": false,
            "calendar": false,
            },
        "change": function(e, tab){
            e && e.preventDefault();
            for(var i in this.tabs){
                this.tabs[i] = i === tab;
            }
        },
    }

    $scope.calendar = new Calendar()

    $scope.schedule = new Schedule($scope);

}]);