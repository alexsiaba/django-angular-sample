dashboardApp.controller('companyController',
['$scope', '$http', 'SharedData', 'Collection', 'objects', 'alert',
function($scope, $http, SharedData, Collection, objects, alert){

    function hideAlert(){
        $scope.$apply(function(){
            $scope.alert.show = false
        });
    }

    function employee_sorting(a,b){
        if(a.given_name > b.given_name){
            return 1;
        }
        return -1;
    }

    function Company(args){
        args = args || {}

        this.departments = new Collection(objects.Department, args['departments'], 'department')
        this.devices = new Collection(objects.Device, args['devices'], 'device')
        this.employees = new Collection(objects.Employee, args['employees'], 'employee', employee_sorting)
        this.jobs = new Collection(objects.Job, args['jobs'], 'job')
        this.settings = new objects.Settings(args['settings'])
        this.save = function(){
            var $this = this;
            if(this.settings.id){
                promise = $http.put('/api/v1/company/' + $this.settings.id + '/', $this.settings);
            } else {
                promise = $http.post('/api/v1/company/', $this.settings)
            }
            promise
            .success(function(data){
                $this.settings._construct(data);
                $scope.tabs.change(null, 'settings');
                data.severity = 'success'
                data.msg = 'Settings for ' + data.name + ' were successfully saved!'
                $scope.alert.init(data, hideAlert);
            })
            .error(function(data){
                data.severity = 'error';
                data.msg = 'Smth went wrong'
                $scope.alert.init(data, hideAlert);
                console.log(data)
            });
        };
        this.removeObject = function(obj){
            if(confirm('Do you really want to delete '+ obj.get_name() + '?')){
                var url = '/api/v1/company/{company_id}/{object}/{object_id}'
                            .replace('{company_id}', $scope.company.settings.id)
                            .replace('{object}', obj._collection.api_name)
                            .replace('{object_id}', obj.id)
                $http.delete(url).success(function(data){
                    var idx = obj._collection.find(data.id, true)
                    obj._collection.items.splice(idx, 1);
                })
            }
        }
        this.saveObject = function(obj){
            if(!(obj instanceof Array)){
                obj = [obj]
            }
            if(obj.length === 0){
                return {"items": []}
            }
            var url = '/api/v1/company/{company_id}/{object}/'
                        .replace('{company_id}', $scope.company.settings.id)
                        .replace('{object}', obj[0]._collection.api_name);
            promise = null
            var collection = []
            var json_collection = []
            for(var i in obj){
                collection.push(obj[i]);
                json_collection.push(obj[i]._validate())
            }
            if(collection.length === 1 && collection[0].id){
                var great_success = obj[0]._collection.api_name + ' was'
                promise = $http.put(url, {"items": json_collection})
            } else {
                var great_success = obj[0]._collection.api_name + 's were'
                promise = $http.post(url, {"items": json_collection})
            }
            promise
            .success(function(data){
                for(var i in collection){
                    collection[i]._construct(data.items[i]);
                }
                data.severity = 'success'
                data.msg = 'The ' + great_success + ' successfully saved!'
                $scope.alert.init(data, hideAlert);
            })
            .error(function(data){
                data.severity = 'danger'
                $scope.alert.init(data, hideAlert);
            });
        }
        this.saveAll = function(){
            var company = {"settings": this.settings}
            for(var i in this){
                if(this[i] instanceof Collection){
                    company[i] = this[i].to_json_collection();
                }
            }
            delete company.devices
            $http.put('/api/v1/company/{id}/'.replace('{id}', this.settings.id), company)
            .success(function(data){
                data.severity = 'success'
                data.msg = 'Everything was successfully saved!'
                $scope.alert.init(data, hideAlert);
            })
            .error(function(data){
                console.log(data)
            });
        };
    };

    $scope.company = null;

    $scope.companyInit = function(args){
        $scope.company = new Company(args)
        SharedData.set('company', $scope.company);
    };

    $scope.tabs = {
        "tabs": {
            'settings': false,
            'employees': false,
            'devices': false,
            'departments': true
            },
        "change": function(e, tab){
            e && e.preventDefault();
            for(var i in this.tabs){
                this.tabs[i] = i === tab;
            }
        },
        "isVisible": function(tab){
            return this.tabs[tab];
        }
    };

    $scope.checkScope = function(){
        console.log($scope);
    };

    $scope.addToCollection = function(object){
        var collection = $scope.company[object + 's'];
        var obj = new collection.obj()
        obj._collection = collection
        collection.items.unshift(obj)
    }

    $scope.save = function(){
        for(var i in $scope.tabs.tabs){
            if($scope.tabs.tabs[i]){
                $scope.company[i].save($scope.company)
            }
        }
    }

    $scope.alert = alert

}]);