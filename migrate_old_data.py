import os
from customer_dashboard import settings
from customer_dashboard.dashboard.models import *
from datetime import datetime


device_mapping = {
    "df40895c-a4e4-44bc-8484-265b12693354": ("kinetic010","FZED441D01AZC501"),
    "363bc3c2-6546-4695-bd9f-589d32c09e17": ("kinetic004","ZZZZ442D01KTJ501"),
    "249d2829-8a7c-4068-a900-8a12a2e28f72": ("kinetic008","FZED442D01KTJ501"),
    "c1b952b7-863e-4019-b428-994624cf9aa9": ("kinetic009","FZED442D01CV5501"),
    "43dd5c94-c50a-468d-81ae-6b829fffbfb2": ("kinetic007","FZED442D01DPH501"),
    "d4e00ede-fe5e-4999-857b-6bc0d00e47a6": ("kinetic006","ZZZY442D01KTJ501"),
    "027c5302-01d3-4fbb-9a40-b7d210b53e11": ("kinetic005","FZED438D00GSJ501"),
    "77935b50-d7da-4a8c-9fb1-ad5845a36444": ("kinetic001","ZZZX442D01KTJ501"),
    "666ee85b-0103-4424-9456-28c8e6cbba5e": ("kinetic012","ZZZW442D01KTJ501"),
}

company_mapping = {
    "4208a20c-3ef2-4f45-b065-25c8a8e60d93": "Crane",
    "b08f0438-fc1b-4d37-a935-3170e5be776b": "UPS"
}

department_mapping = {
    "4ecac71c-1d10-414c-9f55-435a5e925224": ("Unloaders", "b08f0438-fc1b-4d37-a935-3170e5be776b"),
    "56331be8-07ce-4d8f-8efa-3b55dc1cbb33": ("Packers", "4208a20c-3ef2-4f45-b065-25c8a8e60d93"),
    "59cde52e-cc1a-4c59-b6f0-9294408782f4": ("Checkers", "4208a20c-3ef2-4f45-b065-25c8a8e60d93"),
    "cdb42ea6-70a1-4b95-afae-2de098d6fe22": ("Loaders", "b08f0438-fc1b-4d37-a935-3170e5be776b"),
}


for device_former_id, name_id in device_mapping.iteritems():
    Device.objects.create(id=name_id[1],label=device_former_id,name=name_id[0])


for comp_id, comp_name in company_mapping.iteritems():
    Company.objects.create(id=uuid.UUID(comp_id), name=comp_name)

for dept_id, name_company in department_mapping.iteritems():
    dept_name = name_company[0]
    company = Company.objects.get(pk=name_company[1])
    Department.objects.create(id=uuid.UUID(dept_id), name=dept_name, company=company)




with open(os.path.join(settings.BASE_DIR, 'employee.csv')) as f:
    for line in f:
        l = line.replace('"', '')
        employee_raw = l.split(',')

        try:
            company = Company.objects.get(pk=uuid.UUID(employee_raw[12]))
        except ObjectDoesNotExist:
            company = Company.objects.create(id=uuid.UUID(employee_raw[12]), name=company_mapping[employee_raw[12]])

        Employee.objects.create(
            id=uuid.UUID(employee_raw[0]),
            given_name=employee_raw[4],
            dob=employee_raw[8],
            gender=employee_raw[9],
            company=company
        )

with open(os.path.join(settings.BASE_DIR, 'shifts.csv')) as f:
    for line in f:
        l = line.replace('"', '')
        shift_raw = l.split(',')

        start_time = datetime.strptime(shift_raw[4][:-3], "%Y-%m-%d %H:%M.%f").replace(tzinfo=pytz.utc)
        end_time = datetime.strptime(shift_raw[5][:-3], "%Y-%m-%d %H:%M.%f").replace(tzinfo=pytz.utc)

        department = Department.objects.get(pk=uuid.UUID(shift_raw[9]))
        employee = Employee.objects.get(pk=uuid.UUID(shift_raw[10]))
        back_device = Device.objects.get(label=shift_raw[8])


        Shift.objects.create(
            id=uuid.UUID(shift_raw[0]),
            start_time=start_time,
            end_time=end_time,
            notes=shift_raw[9],
            department=department,
            employee=employee,
            back_device=back_device,
            wrist_device=None
        )

